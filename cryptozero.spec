%define dir     cryptozero

Name:           cryptozero
Version:        1.2.1
Release:        2
Summary:        Crypting software for X
Group:          Utilities
License:        GPL Version 2
URL:            http://pay4.free.fr/index.php?page=cryptozero
Source0:        http://pay4.free.fr/dl/cryptozero.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  fpc >= 2.4.2, lazarus >= 1.4.0
Requires:       gtk2

%description

Crypting software for x using GTK 2, can crypt files & strings with 8 ciphers, and is easy-to-use.

%prep
if [ ! -z $RPM_BUILD_DIR ]
then
  cd $RPM_SOURCE_DIR
  tar -xzvf cryptozero.tar.gz -C $RPM_BUILD_DIR
fi

%install
if [ ! -z $RPM_BUILD_DIR ]
then
  if [ ! -z %{dir} ]
  then
    rm $RPM_BUILD_ROOT/usr/bin/* -f
    rm $RPM_BUILD_ROOT/usr/share/%{dir} -Rf
  fi
  mkdir $RPM_BUILD_ROOT/usr
  mkdir $RPM_BUILD_ROOT/usr/bin
  mkdir $RPM_BUILD_ROOT/usr/share
  mkdir $RPM_BUILD_ROOT/usr/share/%{dir}
  mkdir $RPM_BUILD_ROOT/usr/share/applications
  cp $RPM_BUILD_DIR/*.gif $RPM_BUILD_ROOT/usr/share/%{dir}
  cp $RPM_BUILD_DIR/CryptoZero $RPM_BUILD_ROOT/usr/bin
  cp $RPM_BUILD_DIR/*.xpm $RPM_BUILD_ROOT/usr/share/applications
fi

%clean
if [ ! -z $RPM_BUILD_DIR ]
then
  rm -rf $RPM_BUILD_ROOT/
fi

%files
%defattr(-,root,root,-)
/usr/share/%{dir}/
/usr/bin/CryptoZero
/usr/share/applications/cryptozero.xpm

%changelog

* Wed Feb 15 2006 Felipe Monteiro de Carvalho <felipemonteiro.carvalho at gmail.com> - 3.2-mdk.i386.rpm
- The Linux RPM package is created.
