unit AppCfgUnit;

{$mode objfpc}{$H+}

interface

uses Classes, SysUtils, FileUtil, LResources,
{$IFDEF MSWINDOWS}
  Windows,
{$ENDIF}
  Dialogs, DCPrijndael, DCPsha256, DCPmd5, DCPThread, LazUTF8;

const
  basekey = '%*$abcdefghijklmnopqrstuvwxyz<+-/!>ABCDEFGHIJKLMNOPQRSTUVWXYZ@[()]_0123456789';
  SOCKET_ERROR = -1;

type
  PByteArray = ^TByteArray;
  TByteArray = Array of Byte;

//relatif à la conf du soft
 TCoords = record
   top,left,width,height:word;
 end;

 PGeneralConf = ^TGeneralConf;
 TGeneralConf = record
   saveKey, historize, overwrite, showKey, saveList, recurMode {$IFDEF MSWINDOWS},ctxMenu{$ENDIF},upnp:boolean;
   lastCipherStrm: TEncryption;
   lastKeyStrm:string[64];
   lastCipherStr: TEncryption;
   lastKeyStr:string[64];
   formstate: array[1..6] of TCoords;
   lastHost:string[64];
   downloadPath: string[255];
   md5key:array[0..15]of Byte;
 end;

 TOpType = (onFile,onString);

 TTypeKey = record
   mode:Word;
   key:string[64];
 end;

 PHistoKey = ^THistoKey;
 THistoKey = record
   mode:Word;
   key:string[64];
   date:TDateTime;
   used:TOpType;
   info:string[64];
 end;

 TAppConfig = Class(TObject)
   protected
   private
{$IFDEF LINUX}
     Version:string;
{$ENDIF}
     rekey: string;
     home: string;
     fHnd: File of byte;
     re: TDCP_rijndael;
     procedure WriteDefaultConfig();
   public
     HistoKey:array of THistoKey;
     ListFile:TStringList;
     function GetVersion(): string;
     procedure AddToHisto(mode: integer; opType:TOpType; Key,info:string);
     procedure DelFromHisto(id: integer);
     procedure SaveConfig();
     procedure SaveHisto();
     procedure SaveListFile();
     procedure LoadConfig();
     constructor Create();
     destructor Destroy(); override;
   class var
     AppConfig:TGeneralConf;
 end;

 function crc32(crc: cardinal; buf: Pbyte; len: cardinal): cardinal;

var
  conf : TAppConfig;
  Max_Buffer: Cardinal = 1000000;

implementation

{ =========================================================================
  This function can be used by asm versions of crc32() }
{local}
const
  crc32_table : array[Byte] of cardinal = (
  $00000000, $77073096, $ee0e612c, $990951ba, $076dc419,
  $706af48f, $e963a535, $9e6495a3, $0edb8832, $79dcb8a4,
  $e0d5e91e, $97d2d988, $09b64c2b, $7eb17cbd, $e7b82d07,
  $90bf1d91, $1db71064, $6ab020f2, $f3b97148, $84be41de,
  $1adad47d, $6ddde4eb, $f4d4b551, $83d385c7, $136c9856,
  $646ba8c0, $fd62f97a, $8a65c9ec, $14015c4f, $63066cd9,
  $fa0f3d63, $8d080df5, $3b6e20c8, $4c69105e, $d56041e4,
  $a2677172, $3c03e4d1, $4b04d447, $d20d85fd, $a50ab56b,
  $35b5a8fa, $42b2986c, $dbbbc9d6, $acbcf940, $32d86ce3,
  $45df5c75, $dcd60dcf, $abd13d59, $26d930ac, $51de003a,
  $c8d75180, $bfd06116, $21b4f4b5, $56b3c423, $cfba9599,
  $b8bda50f, $2802b89e, $5f058808, $c60cd9b2, $b10be924,
  $2f6f7c87, $58684c11, $c1611dab, $b6662d3d, $76dc4190,
  $01db7106, $98d220bc, $efd5102a, $71b18589, $06b6b51f,
  $9fbfe4a5, $e8b8d433, $7807c9a2, $0f00f934, $9609a88e,
  $e10e9818, $7f6a0dbb, $086d3d2d, $91646c97, $e6635c01,
  $6b6b51f4, $1c6c6162, $856530d8, $f262004e, $6c0695ed,
  $1b01a57b, $8208f4c1, $f50fc457, $65b0d9c6, $12b7e950,
  $8bbeb8ea, $fcb9887c, $62dd1ddf, $15da2d49, $8cd37cf3,
  $fbd44c65, $4db26158, $3ab551ce, $a3bc0074, $d4bb30e2,
  $4adfa541, $3dd895d7, $a4d1c46d, $d3d6f4fb, $4369e96a,
  $346ed9fc, $ad678846, $da60b8d0, $44042d73, $33031de5,
  $aa0a4c5f, $dd0d7cc9, $5005713c, $270241aa, $be0b1010,
  $c90c2086, $5768b525, $206f85b3, $b966d409, $ce61e49f,
  $5edef90e, $29d9c998, $b0d09822, $c7d7a8b4, $59b33d17,
  $2eb40d81, $b7bd5c3b, $c0ba6cad, $edb88320, $9abfb3b6,
  $03b6e20c, $74b1d29a, $ead54739, $9dd277af, $04db2615,
  $73dc1683, $e3630b12, $94643b84, $0d6d6a3e, $7a6a5aa8,
  $e40ecf0b, $9309ff9d, $0a00ae27, $7d079eb1, $f00f9344,
  $8708a3d2, $1e01f268, $6906c2fe, $f762575d, $806567cb,
  $196c3671, $6e6b06e7, $fed41b76, $89d32be0, $10da7a5a,
  $67dd4acc, $f9b9df6f, $8ebeeff9, $17b7be43, $60b08ed5,
  $d6d6a3e8, $a1d1937e, $38d8c2c4, $4fdff252, $d1bb67f1,
  $a6bc5767, $3fb506dd, $48b2364b, $d80d2bda, $af0a1b4c,
  $36034af6, $41047a60, $df60efc3, $a867df55, $316e8eef,
  $4669be79, $cb61b38c, $bc66831a, $256fd2a0, $5268e236,
  $cc0c7795, $bb0b4703, $220216b9, $5505262f, $c5ba3bbe,
  $b2bd0b28, $2bb45a92, $5cb36a04, $c2d7ffa7, $b5d0cf31,
  $2cd99e8b, $5bdeae1d, $9b64c2b0, $ec63f226, $756aa39c,
  $026d930a, $9c0906a9, $eb0e363f, $72076785, $05005713,
  $95bf4a82, $e2b87a14, $7bb12bae, $0cb61b38, $92d28e9b,
  $e5d5be0d, $7cdcefb7, $0bdbdf21, $86d3d2d4, $f1d4e242,
  $68ddb3f8, $1fda836e, $81be16cd, $f6b9265b, $6fb077e1,
  $18b74777, $88085ae6, $ff0f6a70, $66063bca, $11010b5c,
  $8f659eff, $f862ae69, $616bffd3, $166ccf45, $a00ae278,
  $d70dd2ee, $4e048354, $3903b3c2, $a7672661, $d06016f7,
  $4969474d, $3e6e77db, $aed16a4a, $d9d65adc, $40df0b66,
  $37d83bf0, $a9bcae53, $debb9ec5, $47b2cf7f, $30b5ffe9,
  $bdbdf21c, $cabac28a, $53b39330, $24b4a3a6, $bad03605,
  $cdd70693, $54de5729, $23d967bf, $b3667a2e, $c4614ab8,
  $5d681b02, $2a6f2b94, $b40bbe37, $c30c8ea1, $5a05df1b,
  $2d02ef8d);

function get_crc32_table : {const} Pcardinal;
begin
  get_crc32_table :=  {const} Pcardinal(@crc32_table);
end;

{ ========================================================================= }

function crc32 (crc : cardinal; buf : Pbyte; len : cardinal): cardinal;
begin
  if buf = nil then
    exit(0);

  crc := crc xor $FFFFFFFF;
  while (len >= 8) do
  begin
    crc := crc32_table[(crc xor buf^) and $ff] xor (crc shr 8);
    inc(buf);
    crc := crc32_table[(crc xor buf^) and $ff] xor (crc shr 8);
    inc(buf);
    crc := crc32_table[(crc xor buf^) and $ff] xor (crc shr 8);
    inc(buf);
    crc := crc32_table[(crc xor buf^) and $ff] xor (crc shr 8);
    inc(buf);
    crc := crc32_table[(crc xor buf^) and $ff] xor (crc shr 8);
    inc(buf);
    crc := crc32_table[(crc xor buf^) and $ff] xor (crc shr 8);
    inc(buf);
    crc := crc32_table[(crc xor buf^) and $ff] xor (crc shr 8);
    inc(buf);
    crc := crc32_table[(crc xor buf^) and $ff] xor (crc shr 8);
    inc(buf);
    dec(len, 8);
  end;

  while (len > 0) do
  begin
    crc := crc32_table[(crc xor buf^) and $ff] xor (crc shr 8);
    inc(buf);
    dec(len);
  end;

  result := crc xor $FFFFFFFF;
end;

constructor TAppConfig.Create();
var dcpmd5: TDCP_md5;
begin
  inherited Create;
{$IFDEF MSWINDOWS}
  home := SysUtils.GetEnvironmentVariable('APPDATA')+'\cryptozero\';
{$ENDIF}
{$IFDEF LINUX}
  Version := '1.3.3 build 102';
  home := GetEnvironmentVariable('HOME')+'/.cryptozero/';
{$ENDIF}
{$IFDEF MSWINDOWS}
  if not DirectoryExists(home) then
    if not CreateDir(home) then
      ShowMessage('We can''t create folder cryptozero in your appdata folder.');
{$ENDIF}
{$IFDEF LINUX}
  if not FileExists(home) then
    if not CreateDir(home) then
      ShowMessage('Please create folder ~/.cryptozero/ with apropriate access.');
 //Exec cat /proc/sys/net/ipv4/tcp_rmem read/recv
 //         /proc/sys/net/ipv4/tcp_wmem write/send
 //Regepr sur 3 groupe décimal set global Max_Buffer = match[3]
{$ENDIF}
  rekey := GetVersion()+home;
  dcpmd5 := TDCP_md5.Create(nil);
  dcpmd5.Init;
  dcpmd5.UpdateStr(rekey);
  dcpmd5.Final(conf.AppConfig.md5key);
  dcpmd5.Burn;
  dcpmd5.Free;
  ListFile := TStringList.Create;
  Randomize;
end;

destructor TAppConfig.Destroy();
begin
  SaveConfig();

  if conf.AppConfig.historize = true then
    SaveHisto();

  if conf.AppConfig.saveList = true then
    SaveListFile();

  inherited Destroy();
end;

procedure TAppConfig.SaveHisto();
var i,l:integer;
  outData: Pointer;
  inData : THistoKey;
begin
  l := SizeOf(THistoKey);
  outData := GetMem(l);

  re := TDCP_rijndael.Create(nil,nil);
  re.InitStr(rekey, TDCP_sha256);

  AssignFile(fHnd,home+'Histo.dat');
  Rewrite(fHnd);

  for i := 0 to High(HistoKey) do
  begin
    inData := HistoKey[i];
    re.Encrypt(pByte(@inData)^,pByte(outData)^,l);
    BlockWrite(fHnd,outdata^,l);
  end;

  CloseFile(fHnd);

  re.Burn;
  re.Free;
end;

procedure TAppConfig.AddToHisto(mode: integer; opType:TOpType; Key,info:string);
var cnt: integer;
begin
  cnt := Length(HistoKey);
  SetLength(HistoKey,cnt+1);
  HistoKey[cnt].mode := mode;
  HistoKey[cnt].key := Key;
  HistoKey[cnt].info := info;
  HistoKey[cnt].used := opType;
  HistoKey[cnt].date := Date;
end;

procedure TAppConfig.DelFromHisto(id: integer);
var i,cnt: integer;
begin
  cnt := Length(HistoKey);
  for i := id-1 to cnt - 1 do
    HistoKey[i] := HistoKey[i+1];
  SetLength(HistoKey, cnt-1);
end;

procedure CopyMemory(const Src;var Dst; l: Int64);
var
  i: {$IFDEF WIN64}Int64{$ELSE}Integer{$ENDIF};
  ps,pd: pointer;
begin
  ps := @Src;
  pd := @Dst;
  for i := 0 to l - 1 do
  begin
    FillChar(pd^,1,Byte(ps^));
    ps := pointer(ps + 1);
    pd := pointer(pd + 1);
  end;
end;

procedure TAppConfig.SaveConfig();
var outData: Pointer;
  l,r: integer;
begin
  r := 0;
  l := SizeOf(TGeneralConf);

  outData := GetMem(l);

  re := TDCP_rijndael.Create(nil,nil);
  re.InitStr(rekey, TDCP_sha256);
  re.Encrypt(pByte(@AppConfig)^,pByte(outData)^,l);
  re.Burn;
  re.Free;

  AssignFile(fHnd,home+'AppCfg.dat');
  Rewrite(fHnd);
  BlockWrite(fHnd,pByte(outData)^,l,r);
  CloseFile(fHnd);

  Freemem(outData,l);
end;

procedure TAppConfig.WriteDefaultConfig();
begin
  AppConfig.saveKey := false;
  AppConfig.historize := false;
  AppConfig.showKey := true;
  AppConfig.saveList := true;
  AppConfig.lastCipherStr := eBf;
  AppConfig.lastKeyStr := '';
  AppConfig.lastCipherStrm := eBf;
  AppConfig.lastKeyStrm := '';
  AppConfig.upnp := false;
  AppConfig.lastHost := 'Hôte FQDN/IPv4';
  AppConfig.downloadPath:='';
  FillByte(AppConfig.formstate,SizeOf(AppConfig.formstate),0);

  SaveConfig();

  AssignFile(fHnd,home+'Histo.dat');
  Rewrite(fHnd);
  CloseFile(fHnd);

  AssignFile(fHnd,home+'ListFile.dat');
  Rewrite(fHnd);
  CloseFile(fHnd);

end;

procedure TAppConfig.LoadConfig();
var md5key: array[0..15]of Byte;
  l,cnt:integer;
  inData, outData: Pointer;
begin
  if (not FileExists(home+'AppCfg.dat'))
    or (not FileExists(home+'Histo.dat'))
    or (not FileExists(home+'ListFile.dat')) then
      WriteDefaultConfig();

  try
    md5key := AppConfig.md5key;
    l := SizeOf(TGeneralConf);
    inData := GetMem(l);

    if l <> FileSize(home+'AppCfg.dat') then
      raise Exception.Create('File size mismatch.');

    AssignFile(fHnd,home+'AppCfg.dat');
    Reset(fHnd);
    BlockRead(fHnd,pByte(inData)^,l);
    CloseFile(fHnd);

    re := TDCP_rijndael.Create(nil,nil);
    re.InitStr(rekey, TDCP_sha256);
    re.Decrypt(pByte(inData)^, pByte(@AppConfig)^,l);
    re.Burn;
    re.Free;

    Freemem(inData);

    if CompareMem(@md5key[0],@AppConfig.md5key[0],16) = false then
      raise Exception.Create('md5 mismatch');

    except on e:Exception do
    begin
      ShowMessage('Fichier de configuration altéré ou inaccessible'+#13#10+'Réinitialisation de la configuration!');
      AppConfig.md5key := md5key;
      WriteDefaultConfig();
    end;
  end;

    try
  re := TDCP_rijndael.Create(nil,nil);
  re.InitStr(rekey, TDCP_sha256);

  l := SizeOf(THistoKey);
  outData := GetMem(l);
  inData := GetMem(l);

  cnt := 0;
  AssignFile(fHnd, home + 'Histo.dat');
  Reset(fHnd);
  while not eof(fHnd) do
  begin
    SetLength(HistoKey,cnt+1);
    BlockRead(fHnd,pByte(inData)^,l);
    re.Decrypt(pByte(inData)^,pByte(outData)^,l);
    HistoKey[cnt] := PHistoKey(outData)^;
    inc(cnt);
  end;
  CloseFile(fHnd);

  Freemem(inData);
  Freemem(outData);

  re.Burn;
  re.Free;

    except on e:Exception do
    begin
      ShowMessage('Fichier d''historique altéré ou inaccessible'+#13#10+'Réinitialisation de la configuration!');
    end;
  end;

    try
  re := TDCP_rijndael.Create(nil,nil);
  re.InitStr(rekey, TDCP_sha256);

  l := SizeOf(string[255]);
  outData := GetMem(l);
  inData := GetMem(l);

  AssignFile(fHnd, home + 'ListFile.dat');
  Reset(fHnd);
  while not eof(fHnd) do
  begin
    BlockRead(fHnd,pByte(inData)^,l);
    re.Decrypt(pByte(inData)^,pByte(outData)^,l);
    ListFile.Add(PShortString(outData)^);
  end;
  CloseFile(fHnd);

  Freemem(inData);
  Freemem(outData);

  re.Burn;
  re.Free;
    except on e:Exception do
    begin
      ShowMessage('Fichier de file altéré ou inaccessible'+#13#10+'Réinitialisation de la configuration!');
    end;
  end;
end;

procedure TAppConfig.SaveListFile();
var i,l:integer;
  inData: string[255];
  outData: Pointer;
begin
  re := TDCP_rijndael.Create(nil,nil);
  re.InitStr(rekey, TDCP_sha256);

  l := SizeOf(string[255]);
  outData := GetMem(l);

  AssignFile(fHnd,home+'ListFile.dat');
  Rewrite(fHnd);
  for i := 0 to ListFile.Count-1 do
    begin
      inData := ListFile[i];
      re.Encrypt(pByte(@inData)^,pByte(outData)^,l);
      BlockWrite(fHnd,pByte(outData)^,l);
    end;
  CloseFile(fHnd);

  Freemem(outData);

  re.Burn;
  re.Free;
end;

{$ifdef MSWINDOWS}
function TAppConfig.GetVersion(): string;
var
  sFileName: string;
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
begin
  sFileName := ParamStr(0);
  VerInfoSize := GetFileVersionInfoSize(PChar(sFileName), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(sFileName), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    Result := IntToStr(dwFileVersionMS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF);
    Result := Result + '.' + IntToStr(dwFileVersionLS shr 16);
    Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF);
  end;
  FreeMem(VerInfo, VerInfoSize);
end;
{$endif}
{$ifdef LINUX}
function TAppConfig.GetVersion(): string;
begin
  result := Version;
end;
{$endif}


initialization
  conf := TAppConfig.Create();
finalization
  conf.Free;
end.
