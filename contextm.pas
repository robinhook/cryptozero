unit ContextM;

{$mode objfpc}{$H+}

interface

uses
   Windows, ActiveX, ComObj, ShlObj, SysUtils, Classes,
   ComServ, ShellApi, Registry;

type
  TContextMenu = class(TComObject, IShellExtInit, IContextMenu)
  private
    FList: TStrings;   // Liste des fichiers/répertoires seleccionnés ...
  protected
    { IShellExtInit }
    function IShellExtInit.Initialize = SEIInitialize; // Avoid compiler warning
    function SEIInitialize(pidlFolder: PItemIDList; lpdobj: IDataObject;
      hKeyProgID: HKEY): HResult; stdcall;
    { IContextMenu }
    function QueryContextMenu(Menu: HMENU; indexMenu, idCmdFirst, idCmdLast,
      uFlags: UINT): HResult; stdcall;
    function InvokeCommand(var lpici: TCMInvokeCommandInfo): HResult; stdcall;
{$IFDEF WIN64}
    function GetCommandString(idcmd: UINT_Ptr; uType: UINT; pwreserved: puint;
      pszName: LPStr; cchMax: uint): HResult; StdCall;
{$ELSE}
    function GetCommandString(idCmd, uType: UINT; pwReserved: PUINT;
      pszName: LPSTR; cchMax: UINT): HResult; stdcall;
{$ENDIF}
  public
    procedure Initialize; override;
    destructor Destroy; override;
  end;

const
  Class_ContextMenu: TGUID = '{C763DB5D-B6A8-4A21-9B7E-41EF5CBABD56}';

resourcestring
  sPathError = 'Error setting current directory';

implementation

// Exécuté à chaque fois que l' on fait un click pour ouvrir le PopMenu ...
procedure TContextMenu.Initialize;
begin
  inherited;
  FList := TStringList.Create;
end;

// Exécuté à chaque fois que l' on ferme le PopMenu ...
destructor TContextMenu.Destroy;
begin
  inherited Destroy;
  FList.Free;
end;

function TContextMenu.SEIInitialize(pidlFolder: PItemIDList; lpdobj: IDataObject;
  hKeyProgID: HKEY): HResult; stdcall;
var
  StgMedium: TStgMedium;
  FormatEtc: TFormatEtc;
  FileName: array[0..MAX_PATH] of Char;
  i, nbSelectedFiles: Integer;
begin
  // Fail the call if lpdobj is Nil.
  if (lpdobj = nil) then begin
    Result := E_INVALIDARG;
    Exit;
  end;

  with FormatEtc do begin
    cfFormat := CF_HDROP;
    ptd      := nil;
    dwAspect := DVASPECT_CONTENT;
    lindex   := -1;
    tymed    := TYMED_HGLOBAL;
  end;

  // Render the data referenced by the IDataObject pointer to an HGLOBAL
  // storage medium in CF_HDROP format.
  Result := lpdobj.GetData(FormatEtc, StgMedium);
  if Failed(Result) then
    Exit;

  // On a garder les fichiers seleccionnés pour les traiter lorsque
  // l' utilisateur cliquera sur l' item que l' on a crée dans le PopMenu :
  FList.Clear;
  nbSelectedFiles := DragQueryFile(StgMedium.hGlobal, $FFFFFFFF, nil, 0);
  for i := 0 to nbSelectedFiles - 1 do
  begin
    // Récupérons le fichier ou répertoire d' indice "i" dans FFileName:
    DragQueryFile(StgMedium.hGlobal, i, FileName, SizeOf(FileName));
    FList.Add(FileName);
  end;
  FList.SaveToFile('d:\temp\delme.txt');

  Result := NOERROR;
  ReleaseStgMedium(StgMedium);
end;

// Creation du MenuItem dans le PopMenu :
function TContextMenu.QueryContextMenu(Menu: HMENU; indexMenu, idCmdFirst,
          idCmdLast, uFlags: UINT): HResult; stdcall;
begin
  Result := 0; // or use MakeResult(SEVERITY_SUCCESS, FACILITY_NULL, 0);

  if ((uFlags and $0000000F) = CMF_NORMAL) or
     ((uFlags and CMF_EXPLORE) <> 0) then begin
    // Add one menu item to context menu
    InsertMenu(Menu, indexMenu, MF_STRING or MF_BYPOSITION, idCmdFirst,
      'Delphi test ...');

    // Return number of menu items added
    Result := 1; // or use MakeResult(SEVERITY_SUCCESS, FACILITY_NULL, 1)
  end;
end;

// Função executada no OnClick do menu :
function TContextMenu.InvokeCommand(var lpici: TCMInvokeCommandInfo): HResult; stdcall;
begin
  Result := E_FAIL;
  // Make sure we are not being called by an application
  if (HiWord(Integer(lpici.lpVerb)) <> 0) then
  begin
    Exit;
  end;

  // Make sure we aren't being passed an invalid argument number
  if (LoWord(Integer(lpici.lpVerb)) <> 0) then begin
    Result := E_INVALIDARG;
    Exit;
  end;

  // On montre les fichiers/répertoires séleccionnés:
  // FList;
end;

{$IFDEF WIN64}
function TContextMenu.GetCommandString(idcmd: UINT_Ptr; uType: UINT; pwreserved: puint;
  pszName: LPStr; cchMax: uint): HResult; StdCall;
{$ELSE}
function TContextMenu.GetCommandString(idCmd, uType: UINT; pwReserved: PUINT;
  pszName: LPSTR; cchMax: UINT): HRESULT; stdcall;
{$ENDIF}
begin
  if (idCmd = 0) then begin
    if (uType = GCS_HELPTEXT) then
      // return help string for menu item
      StrCopy(pszName, 'Mauricio ContextMenu test');
    Result := NOERROR;
  end
  else
    Result := E_INVALIDARG;
end;

type
  TContextMenuFactory = class(TComObjectFactory)
  public
    procedure UpdateRegistry(Register: Boolean); override;
  end;

procedure TContextMenuFactory.UpdateRegistry(Register: Boolean);
var
  InClassID: string;
  reg: TRegistry;
begin
  try
    if Register then
    begin
//      inherited UpdateRegistry(Register);

      InClassID := GUIDToString(Class_ContextMenu);

      reg := TRegistry.Create();
      reg.RootKey := HKEY_CLASSES_ROOT;
      reg.Access := KEY_ALL_ACCESS;
      if reg.OpenKey('*\shellex\ContextMenuHandlers\CZContextMenu',True) then
        reg.WriteString('', InClassID);
//      reg.CloseKey;
      if reg.OpenKey('\Directory\shellex\ContextMenuHandlers',True) then
        reg.WriteString('', InClassID);
//      reg.CloseKey;
      reg.Free;

      if (Win32Platform = VER_PLATFORM_WIN32_NT) then
      begin
        reg := TRegistry.Create(KEY_ALL_ACCESS);
        try
          reg.RootKey := HKEY_LOCAL_MACHINE;
          reg.OpenKey('SOFTWARE\Microsoft\Windows\CurrentVersion\Shell Extensions', True);
          reg.OpenKey('Approved', True);
          reg.WriteString(InClassID, 'CryptoZero ContextMenu');
        finally
          reg.Free;
        end;
      end;
    end else
    begin
      reg := TRegistry.Create(KEY_ALL_ACCESS);
      reg.RootKey := HKEY_CLASSES_ROOT;
      reg.DeleteKey('*\shellex\ContextMenuHandlers\CZContextMenu');
//      DeleteRegKey('*\shellex\ContextMenuHandlers');
//      DeleteRegKey('*\shellex');
      reg.DeleteKey('Directory\shellex\ContextMenuHandlers\CZContextMenu');
//      DeleteRegKey('Directory\shellex\ContextMenuHandlers');
//      DeleteRegKey('Directory\shellex');
      reg.Free;

      inherited UpdateRegistry(Register);
    end;
    except on e:Exception do
    begin
//      e.Message;
    end;
  end;
end;

initialization
  TContextMenuFactory.Create(ComServer, TContextMenu, Class_ContextMenu,
    'CZContextMenu', 'CryptoZero Shell Extension Menu', ciMultiInstance,
    tmApartment);
end.
