unit DCPThread;

{$mode objfpc}{$H+}

interface

uses
  {$IFDEF UNIX}BaseUnix,{$ENDIF}
  Classes, SysUtils, Forms, ComCtrls, DCPCrypt2, DCPrijndael, DCPdes, DCPsha1, DCPblowfish, DCPserpent, DCPtwofish, DCPrc6, DCPcast256, DCPice, Dialogs;

const
  dcpMCrypt = 0;
  dcpMDecrypt = 1;

Type
    TEncryption = (eBf, eCast256, eIce2, e3Des, eRdl, eRc6, eSerpent, eTwofish);
    TProgressRecord = record
        Mode:byte;
        EKey:TEncryption;
        Key,FilenameIn,FilenameOut: String;
        CurSz,TotSz,lastPos: Int64;
        pbFile,pbMultiFiles: TProgressBar;
    end;

    TDCPThread = class(TThread)
    private
      ThrFinished: boolean;
      Sz,Pos: Int64;
      pbFilePos,pbMultiFilePos: integer;
      procedure UpdateStatus;
    protected
      FThrTerminated: boolean;
      procedure CryptFile;
      procedure DecryptFile;
      procedure Execute; override;
    public
      ProgRec: TProgressRecord;
      DCPCipher : TDCP_cipher;
      Constructor Create(CreateSuspended : boolean;  Params: TProgressRecord);
      procedure CancelOperation();
      procedure Progress(position,size:LongWord);
      procedure ThreadTerminate(Sender:Tobject);
      property Finished: boolean read ThrFinished;
    published
      property Terminated: boolean read FThrTerminated;
    end;

    TThreadComponent = class (TComponent)
    public
      FThr: TDCPThread;
      Constructor Create(AOwner: TComponent; CreateSuspended : boolean; Params: TProgressRecord);
    end;

implementation
  {$R-}{$Q-}

  Constructor TThreadComponent.Create(AOwner: TComponent; CreateSuspended : boolean;  Params: TProgressRecord);
  begin
    inherited Create(AOwner);
    FThr := TDCPThread.Create(CreateSuspended, Params);
  end;

  constructor TDCPThread.Create(CreateSuspended : boolean; Params: TProgressRecord);
  begin
    ProgRec := Params;
    Priority := tpLower;
    pbFilePos := 0;
    if Assigned(ProgRec.pbMultiFiles) then
      pbMultiFilePos := ProgRec.pbMultiFiles.Position;
    inherited Create(CreateSuspended);
  end;

  procedure TDCPThread.ThreadTerminate(Sender: TObject);
  begin
    ThrFinished := true;
  end;

  // Synchronized with main thread method
  procedure TDCPThread.UpdateStatus;
  begin
    ProgRec.pbFile.Position := pbFilePos;
    ProgRec.pbMultiFiles.Position := pbMultiFilePos;
  end;

  procedure TDCPThread.Progress(position,size: LongWord);
  var needSync: boolean;
  begin
    Sz := size;
    Pos := position;
    needSync := false;

    if (Pos > 0) and (Sz > 0) then
    begin
      if Pos < ProgRec.lastPos then
        ProgRec.lastPos := 0;
      pbFilePos := Round((Pos / Sz)*100);
      if (pbFilePos <> ProgRec.pbFile.Position) then
        needSync := true
        else Exit;
      ProgRec.curSz := ProgRec.curSz + (Pos - ProgRec.lastPos);
      ProgRec.lastPos := Pos;
      if((ProgRec.curSz > 0) and (ProgRec.totSz > 0)) then
        pbMultiFilePos := Round((ProgRec.curSz / ProgRec.totSz)*100);
    end else
    begin
      ProgRec.pbFile.Position := 0;
      ProgRec.lastPos := 0;
      needSync := true;
    end;

    if needSync then
      Synchronize(@UpdateStatus);

  end;

  procedure TDCPThread.CryptFile;
  var readStr,writeStr:TFileStream;
  begin
    try
      case ProgRec.EKey of
        eBf      :
        begin
          DCPCipher := TDCP_blowfish.Create(nil,Self);
          TDCP_blowfish(DCPCipher).OnMethProgress := @Progress;
        end;
        eRdl:
        begin
          DCPCipher := TDCP_rijndael.Create(nil,Self);
          TDCP_rijndael(DCPCipher).OnMethProgress := @Progress;
        end;
        e3des:
        begin
          DCPCipher := TDCP_3des.Create(nil,Self);
          TDCP_3des(DCPCipher).OnMethProgress := @Progress;
        end;
        eCast256:
        begin
          DCPCipher := TDCP_cast256.Create(nil,Self);
          TDCP_cast256(DCPCipher).OnMethProgress := @Progress;
        end;
        eIce2:
        begin
          DCPCipher := TDCP_ice2.Create(nil,Self);
          TDCP_ice2(DCPCipher).OnMethProgress := @Progress;
        end;
        eRc6:
        begin
          DCPCipher := TDCP_rc6.Create(nil,Self);
          TDCP_rc6(DCPCipher).OnMethProgress := @Progress;
        end;
        eSerpent:
        begin
          DCPCipher := TDCP_serpent.Create(nil,Self);
          TDCP_serpent(DCPCipher).OnMethProgress := @Progress;
        end;
        eTwofish:
        begin
          DCPCipher := TDCP_twofish.Create(nil,Self);
          TDCP_twofish(DCPCipher).OnMethProgress := @Progress;
        end;
      end;
      DCPCipher.InitStr(ProgRec.Key,TDCP_sha1);
      readStr := TFileStream.Create(ProgRec.FilenameIn,fmOpenRead,fmShareCompat);
      writeStr := TFileStream.Create(ProgRec.FilenameOut,fmCreate,fmShareCompat);
      DCPCipher.EncryptStream(readStr,writeStr,readStr.Size);
    finally
      DCPCipher.Burn;
      DCPCipher.Free;
      writeStr.Free;
      readStr.Free;
      {$ifdef UNIX}
      if FpAccess(ProgRec.FilenameOut,W_OK) <> 0 then
        FpChmod(ProgRec.FilenameOut,&660);
      {$endif}
    end;
  end;

  procedure TDCPThread.DecryptFile();
  var readStr,writeStr:TFileStream;
  begin
    try
      case ProgRec.EKey of
        eBf      :
        begin
          DCPCipher := TDCP_blowfish.Create(nil,Self);
          TDCP_blowfish(DCPCipher).OnMethProgress := @Progress;
        end;
        eRdl:
        begin
          DCPCipher := TDCP_rijndael.Create(nil,Self);
          TDCP_rijndael(DCPCipher).OnMethProgress := @Progress;
        end;
        e3des:
        begin
          DCPCipher := TDCP_3des.Create(nil,Self);
          TDCP_3des(DCPCipher).OnMethProgress := @Progress;
        end;
        eCast256:
        begin
          DCPCipher := TDCP_cast256.Create(nil,Self);
          TDCP_cast256(DCPCipher).OnMethProgress := @Progress;
        end;
        eIce2:
        begin
          DCPCipher := TDCP_ice2.Create(nil,Self);
          TDCP_ice2(DCPCipher).OnMethProgress := @Progress;
        end;
        eRc6:
        begin
          DCPCipher := TDCP_rc6.Create(nil,Self);
          TDCP_rc6(DCPCipher).OnMethProgress := @Progress;
        end;
        eSerpent:
        begin
          DCPCipher := TDCP_serpent.Create(nil,Self);
          TDCP_serpent(DCPCipher).OnMethProgress := @Progress;
        end;
        eTwofish:
        begin
          DCPCipher := TDCP_twofish.Create(nil,Self);
          TDCP_twofish(DCPCipher).OnMethProgress := @Progress;
        end;
      end;
      DCPCipher.InitStr(ProgRec.Key,TDCP_sha1);
      readStr := TFileStream.Create(ProgRec.FilenameIn,fmOpenRead,fmShareCompat);
      writeStr := TFileStream.Create(ProgRec.FilenameOut,fmCreate,fmShareCompat);
      DCPCipher.DecryptStream(readStr,writeStr,readStr.Size);
    finally
      DCPCipher.Burn;
      DCPCipher.Free;
      writeStr.Free;
      readStr.Free;
      {$ifdef UNIX}
      if FpAccess(ProgRec.FilenameOut,W_OK) <> 0 then
        FpChmod(ProgRec.FilenameOut,&660);
      {$endif}
    end;
  end;

  procedure TDCPThread.CancelOperation();
  begin
    DCPCipher.CancelOperation();
  end;

  procedure TDCPThread.Execute;
  begin
    if (ProgRec.Mode = dcpMCrypt) then
      CryptFile()
    else if (ProgRec.Mode = dcpMDecrypt) then
      DecryptFile();
    ThreadTerminate(Self);
  end;

end.

