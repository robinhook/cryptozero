unit UDragDropMemo;

{$mode objfpc}{$H+}

interface

uses
 SysUtils, Classes, Controls, Forms, StdCtrls,
 Windows,
 ComObj, ActiveX, ShlObj, Dialogs;

const
//  CF_Acceptable:          Array [0..1] of String =   ('RenPrivateMessages', 'Internet Message (rfc822/rfc1522)');
  CF_Acceptable:          Array [0..2] of String = ('Text','OEMText','Unicode Text');

// Predefined clipboard formats
const
  CF_PredfinedFormats:    Array [1..15] of String =  ('Text',
                                                      'Bitmap',
                                                      'MetaFile',
                                                      'SYLK',
                                                      'DIF',
                                                      'TIFF',
                                                      'OemText',
                                                      'DIB',
                                                      'Palette',
                                                      'Pen Data',
                                                      'RIFF',
                                                      'Wave',
                                                      'Unicode Text',
                                                      'Enhanced Metafile',
                                                      'HDrop');

implementation

end.

