#
# Parses command line options. Currently supported options are:
#
# DESTDIR		Destination root directory
# 

DESTDIR=""

for arg; do

  case $arg in

    DESTDIR=*) DESTDIR=${arg#DESTDIR=};;

  esac;

done

#
# Does the install
#
# "mkdir -p" is equivalent to ForceDirectories pascal function
#

mkdir -p $DESTDIR/usr/share/cryptozero

cp ./in.gif $DESTDIR/usr/share/cryptozero/
cp ./out.gif $DESTDIR/usr/share/cryptozero/
cp ./CryptoZero.ico $DESTDIR/usr/share/cryptozero/

mkdir -p $DESTDIR/usr/bin

cp ./CryptoZero $DESTDIR/usr/bin/
