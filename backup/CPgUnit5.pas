unit CPgUnit5;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, LCLType, Forms, Controls, Graphics,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, CPSocks, UPnP,
  AppCfgUnit, LazUTF8, TASeries, TAGraph, TAIntervalSources, TAStyles;

type
  { TfrmNetwork }

  TfrmNetwork = class(TForm)
    btStartSend: TButton;
    btListen: TButton;
    btCancelSend: TButton;
    btCancelRecv: TButton;
    cbCipher: TComboBox;
    chStylesRecv: TChartStyles;
    chRecv: TChart;
    chStylesSend: TChartStyles;
    chSend: TChart;
    dtIChSrcRecv: TDateTimeIntervalChartSource;
    dtIChSrcSend: TDateTimeIntervalChartSource;
    edHost: TEdit;
    edKey: TEdit;
    edPort: TEdit;
    lbFNameRecv: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lbFNameSend: TLabel;
    lbRecvStates: TListBox;
    lbSendStates: TListBox;
    pgCtrlNet: TPageControl;
    pbRecv: TProgressBar;
    pbSend: TProgressBar;
    tmCleaner: TTimer;
    tsSend: TTabSheet;
    tsRecv: TTabSheet;
    procedure btCancelRecvClick(Sender: TObject);
    procedure btCancelSendClick(Sender: TObject);
    procedure btListenClick(Sender: TObject);
    procedure btStartSendClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure tmCleanerTimer(Sender: TObject);
  private
    { private declarations }
    FUPnP:TUPnP;
    FSendTerminate: boolean;
    FChAreaSerSend, FChAreaSerRecv: TAreaSeries;
    FCancelSend,FCancelRecv: boolean;
    procedure SendTcp(Host:string; Port:word);
   public
    { public declarations }
    procedure DoBtCancelVisible;
    procedure ProcessSockMsgQueue;
    procedure ProcessRecvRequest;
    procedure ResetRecvUI;
    procedure UpdateProgressRecv;
    procedure ProcessSendRequest;
    procedure ResetSendUI;
    procedure UpdateProgressSend;
    procedure SendTerminate(Sender: TObject);
    procedure waitForMs(ms: integer);

    procedure SetCancelSend(const csend: boolean);
    procedure SetCancelRecv(const crecv: boolean);

    property CancelSend: boolean read FCancelSend write SetCancelSend;
    property CancelRecv: boolean read FCancelRecv write SetCancelRecv;
  end;

var
  frmNetwork: TfrmNetwork;

implementation

uses CPgUnit4, CPgUnit6, RegExpr, Process, Sockets, TACustomSeries, TAChartUtils;

{ TfrmNetwork }
procedure TfrmNetwork.FormCreate(Sender: TObject);
begin
  if (conf.AppConfig.formstate[5].top > 0) and (conf.AppConfig.formstate[5].left > 0) then
  begin
    Top := conf.AppConfig.formstate[5].top;
    Left := conf.AppConfig.formstate[5].left;
  end;
  if (conf.AppConfig.formstate[5].width > 0) and (conf.AppConfig.formstate[5].height > 0) then
  begin
    Width := conf.AppConfig.formstate[5].width;
    Height := conf.AppConfig.formstate[5].height;
  end;

  lbFNameSend.Visible:=false;
  pbSend.Visible:=false;

  lbFNameRecv.Visible:=false;
  pbRecv.Visible:=false;

  lbFNameRecv.Caption := '';
  pbRecv.Position:=0;

  lbFNameSend.Caption := '';
  pbSend.Position:=0;
  if not conf.AppConfig.showKey then
    edKey.PasswordChar := '*';

  if not DirectoryExists(conf.AppConfig.downloadPath) then
    conf.AppConfig.downloadPath := '';

  if conf.AppConfig.lastHost <> '' then
    frmNetwork.edHost.Text := conf.AppConfig.lastHost;

  tsRecv.Color := TColor($C0C0F0);
  tsSend.Color := TColor($C0F0C0);

  FChAreaSerSend := TAreaSeries.Create(chSend);
  FChAreaSerRecv := TAreaSeries.Create(chRecv);

  FChAreaSerSend.Styles := chStylesSend;
  FChAreaSerRecv.Styles := chStylesRecv;

  chSend.AddSeries(FChAreaSerSend);
  chRecv.AddSeries(FChAreaSerRecv);
end;

procedure TfrmNetwork.FormDestroy(Sender: TObject);
begin
  conf.AppConfig.lastHost := frmNetwork.edHost.Text;
end;

procedure TfrmNetwork.FormHide(Sender: TObject);
begin
  conf.AppConfig.formstate[5].top := Top;
  conf.AppConfig.formstate[5].left := Left;
  conf.AppConfig.lastHost := edHost.Text;
//  conf.AppConfig.formstate[3].width := Width;
//  conf.AppConfig.formstate[3].height := Height;
end;

procedure TfrmNetwork.FormResize(Sender: TObject);
begin
  lbFNameRecv.Width := frmNetwork.ClientWidth - 80;

  lbFNameSend.Width := frmNetwork.ClientWidth - 80;

  chRecv.Top := tsRecv.ClientHeight - (lbRecvStates.Top + Round(lbRecvStates.Height / 2)) + 24;
  chRecv.Width := tsRecv.ClientWidth - 4;
  chRecv.Height := tsRecv.ClientHeight - chRecv.Top - 2;

  chSend.Top := tsSend.ClientHeight - (lbSendStates.Top + Round(lbSendStates.Height / 2)) + 24;
  chSend.Width := tsSend.ClientWidth - 4;
  chSend.Height := tsSend.ClientHeight - chSend.Top - 2;

  lbRecvStates.Width := frmNetwork.ClientWidth - 8;
  lbRecvStates.Height := chRecv.Top - lbRecvStates.Top - 4;

  lbSendStates.Width := frmNetwork.ClientWidth - 8;
  lbSendStates.Height := chSend.Top - lbSendStates.Top - 4;
end;

procedure TfrmNetwork.tmCleanerTimer(Sender: TObject);
begin
  if CancelRecv then
    ResetRecvUI;
  if CancelSend then
    ResetSendUI;
  tmCleaner.Enabled := False;
end;

procedure TFrmNetwork.SetCancelSend(const csend: boolean);
begin
  FCancelSend:=csend;
  if FCancelSend = true then
    sendSock.Synchronize(sendSock,@sendSock.SetCancelXFer);
end;

procedure TFrmNetwork.SetCancelRecv(const crecv: boolean);
begin
  FCancelRecv:=crecv;
  if FCancelRecv = true then
    recvSock.Synchronize(recvSock,@recvSock.SetCancelXFer);
end;

procedure TFrmNetwork.ProcessSockMsgQueue;
var i,l:longint;
  sock:TTCPThrd;
  lbx :TListBox;
begin
  sock := nil;
  if Assigned(recvSock) and not recvSock.Finished then
  begin
    sock := recvSock;
    lbx := lbRecvStates;
  end
  else if Assigned(sendSock) and not sendSock.Finished then
  begin
    sock := sendSock;
    lbx := lbSendStates;
  end
  else if Assigned(listenSock) and not listenSock.Finished then
  begin
    sock := listenSock;
    lbx := lbRecvStates;
  end;

  if sock = nil then
  begin
    Exit;
  end;

  try
    l := Length(sock.FMsgsQueue);
    if l > 0 then
    begin
      for i := 0 to l - 1 do
      begin
        lbx.Items.Add(sock.FMsgsQueue[i]);
      end;
      SetLength(sock.FMsgsQueue,0);
    end;
  except on e: Exception do
    lbx.Items.Add('[TFrmNetwork.ProcessMsgQueue] Exception: ' + e.Message);
  end;
end;

procedure TfrmNetwork.ProcessRecvRequest;
begin
  lbFNameRecv.Caption := '';
  lbFNameRecv.Visible := true;
  pbRecv.Position := 0;
  pbRecv.Visible := true;
  lbFNameRecv.Caption := recvSock.GetHeaderBuffer.filename;
  frmQuestion.Question;
end;

procedure TfrmNetwork.ResetRecvUI;
begin
  lbFNameRecv.Caption := '';
  pbRecv.Position:=0;
  pbRecv.Visible := false;
  btCancelRecv.Visible := false;
  CancelSend := False;
end;

procedure TfrmNetwork.DoBtCancelVisible;
begin
  btCancelRecv.Visible := true;
end;

procedure TfrmNetwork.UpdateProgressRecv;
var i: Integer;
  Y: Double;
begin
  if recvSock.NeedSyncPb = true then
  begin
    pbRecv.Position := recvSock.FPcProgress;
  end;
  if recvSock.NeedSyncCh = true then
  begin
    if FChAreaSerRecv.Count < 60 then
      FChAreaSerRecv.AddXY(FChAreaSerRecv.Count,recvSock.GraphRate div 1024)
    else
    begin
      for i := 0 to 58 do
      begin
        Y := FChAreaSerRecv.GetYValue(i + 1);
        FChAreaSerRecv.SetYValue(i,Y);
      end;
      FChAreaSerRecv.SetYValue(59,recvSock.GraphRate div 1024);
    end;
  end;
end;

procedure TfrmNetwork.ProcessSendRequest;
begin
  lbFNameSend.Caption := ExtractFileName(sendSock.GetHeaderBuffer.filename);
  lbFNameSend.Visible := true;
  pbSend.Position := 0;
  pbSend.Visible := true;
end;

procedure TfrmNetwork.ResetSendUI;
begin
  lbFNameSend.Caption := '';
  lbFNameSend.Visible := false;
  pbSend.Position := 0;
  pbSend.visible := false;
  CancelSend := False;
end;

procedure TfrmNetwork.UpdateProgressSend;
var i: integer;
  Y: Double;
begin
  if sendSock.NeedSyncPb = true then
  begin
    pbSend.Position := sendSock.FPcProgress;
  end;
  if sendSock.NeedSyncCh = true then
  begin
    if FChAreaSerSend.Count < 60 then
      FChAreaSerSend.AddXY(FChAreaSerSend.Count,sendSock.GraphRate div 1024)
    else
    begin
      for i := 0 to 58 do
      begin
        Y := FChAreaSerSend.GetYValue(i + 1);
        FChAreaSerSend.SetYValue(i, Y);
      end;
      FChAreaSerSend.SetYValue(59,sendSock.GraphRate div 1024);
    end;
  end;
end;

procedure TfrmNetwork.waitForMs(ms:integer);
var tms,currTms: Comp;
  cycMs: int32;
begin
  tms := TimeStampToMSecs(DateTimeToTimeStamp(Now));
  currTms := tms;
  while tms + ms  > currTms do
  begin
    Application.ProcessMessages;
    if (ms > 249) and (ms < 500) then
      cycMs := 250 div 4
    else if (ms > 499) and (ms < 1000) then
      cycMs := 500 div 7
    else if (ms > 999) and (ms < MaxInt) then
      cycMs := 1000 div 11
    else cycMs := 50;
    Sleep(cycMs);
    currTms := TimeStampToMSecs(DateTimeToTimeStamp(Now));
  end;
end;

procedure TfrmNetwork.SendTerminate(Sender: TObject);
begin
  FSendTerminate := true;
end;

procedure TfrmNetwork.SendTcp(Host:string; Port:word);
var allXFers: boolean;
  i: integer;
  lsFiles:TStringList;
begin
  try
    allXFers := true;
    lsFiles := TStringList.Create;
    lsFiles.SetText(frmQueue.lbFiles.Items.GetText);
    //Boucle fichiers en listes
    for i := 0 to lsFiles.Count - 1 do
    begin
      if FileSize(lsFiles[i]) <= 0 then
      begin
        lbSendStates.Items.Add('Envoi de "'+ExtractFileName(lsFiles[i])+'" impossible!');
        Continue;
      end;
      lbSendStates.Items.Add('Envoi de "'+ExtractFileName(lsFiles[i])+'" ...');

      CancelSend:=false;
      FSendTerminate:=false;
      sendSock := TTCPSendThrd.Create(Port,lsFiles[i]);
      sendSock.setUIComponent(lbFNameSend,pbSend,lbSendStates, cbCipher, edHost, edKey, nil);
      sendSock.Start;

      btCancelSend.Visible:=true;

      while not FSendTerminate and not sendSock.XFerCancel and not sendSock.XFerDone do
        waitForMs(250);

      if sendSock.XFerCancel then
        allXFers := false;

      btCancelSend.Visible := false;
      waitForMs(100);
    end;

    btStartSend.Enabled:=true;
    if allXFers then
      lbSendStates.Items.Add('Les fichiers ont été transféré!')
    else lbSendStates.Items.Add('Tous les fichiers n''ont pu être transféré!');

  finally
    lbFNameSend.Caption:='';
    pbSend.Position:=0;
    pbSend.Visible:=false;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfrmNetwork.btStartSendClick(Sender: TObject);
var port: integer;
begin
  if ((edHost.Text <> '') and (Length(edHost.Text) > 3) and (TryStrToInt(edPort.Text, port))) then
  begin
      lbSendStates.Items.Add('Début de l''envoi des fichiers ...');
      btStartSend.Enabled := false;
      SendTcp(edHost.Text, port);
  end;
end;

procedure TfrmNetwork.btListenClick(Sender: TObject);
var uuid: string;
  i, cnt, port: LongInt;
  rSck,errCode: int32;
  optLen: ^TSockLen;
begin
  //Find local ip that is routed to upnp the listen port
  //ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*'|grep -v 127.0.0.1
  port := 0;
  if((btListen.Caption = 'Ecouter') and (edHost.Text <> '') and (Length(edHost.Text) > 3) and TryStrToInt(edPort.Text,port) and (port > 1025)) then
  begin
     if conf.AppConfig.upnp then
     begin
       FUPnP := TUPnP.Create(edHost.Text,TStringList(lbRecvStates.Items));
       FUPnP.searchDevice;
       if FUPnP.FListDevice.Count > 0 then
         for i := 0 to FUPnP.FListDevice.Count - 1 do
         begin
           if FUPnP.setMapping(FUPnP.FListDevice.Strings[i],'CryptoZero',port) then
             lbRecvStates.AddItem('UPnP device "'+uuid+'"'+#13#10+'has added "CryptoZero" rule.',nil);
           FUPnP.listMapping(FUPnP.FListDevice.Strings[i]);
         end;
       FUPnP.Free;
     end;

    listenSock := TTCPListenThrd.Create(port);
    listenSock.setUIComponent(lbFNameRecv,pbRecv,lbRecvStates,cbCipher,edHost,edKey, btCancelRecv);
    listenSock.Start;

    waitForMs(50);
    if not listenSock.Finished then
    begin
      btListen.Caption := 'Stopper';
      lbRecvStates.Items.Add('En écoute ...');
    end;
  end else if (btListen.Caption = 'Stopper') then
  begin
    if conf.AppConfig.upnp then
    begin
      FUPnP := TUPnP.Create(edHost.Text,TStringList(lbRecvStates.Items));
      if (TUPnP.FLDev <> nil) and (TUPnP.FLDev.Count > 0) then
        FUPnP.restoreDevices
      else FUPnP.searchDevice;
      if FUPnP.FListDevice.Count > 0 then
        for i := FUPnP.FListDevice.Count - 1 downto 0 do
        begin
          uuid := FUPnP.FListDevice[i];
          if FUPnP.unsetMapping(FUPnP.FListDevice[i],'CryptoZero') then
            lbRecvStates.AddItem('UPnP device "'+uuid+'"'+#13#10+'has deleted "CryptoZero" rule.',nil);
        end;
      FUPnP.Free;
    end;

    listenSock.Shutdown;
    cnt := 0;
    while cnt < 10 do
    begin
      rSck := fpgetsockopt(listenSock.ListenSocket, SOL_SOCKET, SO_ERROR , @errCode, optLen);
      if (rSck = SOCKET_ERROR) or (errCode <> 0) then
      begin
        if errCode <> 0 then
          lbRecvStates.Items.Add('[TFrmNetwork.btListenClick] Checking socket state results in an error:'+#13#10
            +'  '+SysErrorMessageUTF8(errCode));
        lbRecvStates.Items.Add('Ecoute terminé.');
        Break;
      end;
      waitForMs(66);
      Inc(cnt);
    end;
    btListen.Caption := 'Ecouter';
  end;
end;

procedure TfrmNetwork.btCancelRecvClick(Sender: TObject);
begin
  CancelRecv:=true;
  tmCleaner.Enabled := True;
end;

procedure TfrmNetwork.btCancelSendClick(Sender: TObject);
begin
  CancelSend:=true;
  tmCleaner.Enabled := True;
end;

initialization
  {$I CPgUnit5.lrs}
end.

