unit CPSocks;

{$mode objfpc}{$H+}

interface

uses {$IFDEF MSWINDOWS}Windows, {$IFDEF FPC}WinSock2{$ELSE}WinSockDelphi{$ENDIF}
  {$ELSE}BaseUnix, Unix, TermIO, NetDB{$ENDIF},
  Classes, SysUtils, Sockets, LCLType, Forms, Controls, Graphics, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls, DCPrijndael, DCPdes, DCPsha1, DCPblowfish, DCPserpent,
  DCPtwofish, DCPrc6, DCPcast256, DCPice, AppCfgUnit, LazUTF8;

const SOCKET_ERROR = -1;

type
  TObjMeth = procedure of object;
  PObjMeth = ^TMethod;
  TTypeBuffer = (tbHeader = 1, tbResponse = 2, tbData = 4, tbClose = 8);
  TNetBufferData = array [0..16356] of byte;

  TNetBuffer = record
    pckNum: Cardinal;
    case tBuf: TTypeBuffer of
      tbClose, tbResponse:
        (value: byte);
      tbHeader:
        (filename:string[128];
        filesize:int64);
      tbData:
        (firstPck, lastPck: Boolean;
        crc32,dataLen:Cardinal;
        datas:TNetBufferData);
  end;

  TTCPThrd = class(TThread)
    private
      FPort             : Word;
      FErrorCode        : Longint;
      btCancel          : TButton;
      lbFNTxt           : TLabel;
      lbFNMsg           : TListBox;
      pbXfer            : TProgressBar;
      cbCipher          : TComboBox;
      edKey,edHost      : TEdit;
    public
      FPcProgress       : byte;
      FMsgsQueue        : array of string;
      procedure setUIComponent(lbl: TLabel; pb:TProgressBar; lbx:TListBox; cbC: TComboBox; edH,edK:TEdit; btC:TButton);
      Procedure AddMsg2Q(Msg: string);
      Procedure PrintMsgsFromQ;
      Procedure PrintError(const prefixStr: string='');
      function TryResolve(HostName: string;var ina:TInAddr):boolean;
      procedure waitForMs(ms:integer);
  end;


  TTCPListenThrd = class(TTCPThrd)
  private
    FListenSocket    : LongInt;
    FServerAddr      : TInetSockAddr;
    FShutdown        : boolean;
  public
    Constructor Create(Port: Word);
    Destructor Destroy; override;
    procedure Shutdown();
    procedure Execute; override;
    property ListenSocket: LongInt read FListenSocket write FListenSocket;
  end;

  TTCPCliThrd = class(TTCPThrd)
    private
      FMaxScrambleSz    : integer; static;
      FXFerCancel       : boolean;
      FXFerDone         : boolean;
      FNeedSyncPb       : boolean;
      FNeedSyncCh       : boolean;
      FClientAddr       : TInetSockAddr;
      FClientSocket     : LongInt;
      FPckNum           : Cardinal;
      FCount            : Int64;

      FKey              : String;
      FCIdx             : Longint;
      FDCPrijndael      : TDCP_rijndael;
      FDCP3des          : TDCP_3des;
      FDCPblowfish      : TDCP_blowfish;
      FDCPserpent       : TDCP_serpent;
      FDCPtwofish       : TDCP_twofish;
      FDCPrc6           : TDCP_rc6;
      FDCPcast256       : TDCP_cast256;
      FDCPice2          : TDCP_ice2;

      FHdrBuff          : TNetBuffer;
      FLastTickPb       : Int64;
      FLastTickCh       : Int64;
{*      FFirstPacket,FLastPacket      : boolean;  *}
      FScramble : array of Byte;
      FScrambleSz : word;
      FGraphRate: LongInt;
      FBufSz,FDtSz: integer;

      procedure SetCipher;
      procedure UnsetCipher;
      function DecryptBuffer(const BuffIn:TNetBuffer;bufSz:cardinal):TNetBuffer;
      function CryptBuffer(const BuffIn:TNetBuffer;bufSz:cardinal):TNetBuffer;

      procedure SetSockBuff(so: UInt32; bSz: UInt32); virtual;
      procedure SetSockSndTmo(tmo: {$IFDEF UNIX}Timeval{$ELSE}Longint{$ENDIF}); virtual;
      procedure SetSockRcvTmo(tmo: {$IFDEF UNIX}Timeval{$ELSE}Longint{$ENDIF}); virtual;

      function WaitingData : cardinal;
      function CanRead(Timeout : Integer) : Boolean;
      function CanWrite(Timeout: integer): boolean;

      procedure UpdateProgress(Meth: string);
    public
      constructor Create(CreateSuspended: Boolean; const StackSize: SizeUInt=DefaultStackSize);
      function GetHeaderBuffer:TNetBuffer;
      procedure SetCancelXFer;

      property XFerCancel: boolean read FXFerCancel;
      property XFerDone: boolean read FXFerDone;
      property GraphRate: longint read FGraphRate;
      property NeedSyncPb: boolean read FNeedSyncPb write FNeedSyncPb;
      property NeedSyncCh: boolean read FNeedSyncCh write FNeedSyncCh;
  end;

  TTCPRecvThrd = class(TTCPCliThrd)
  private
    FPrvPckNum: LongInt;
    FFileName : String;
    FHnd      : File of Byte;
    FLastResp : byte;
    FRcvBuff  : TNetBuffer;
  public
    Host: String;
    waitForAnswer:PRTLEvent;
    Constructor Create(ClientSocket: Longint; ClientAddr:TInetSockAddr);
    Destructor Destroy; override;
    function RecvRequest():boolean;
    procedure Execute; override;
  end;

  TTCPSendThrd = class(TTCPCliThrd)
  private
    FSndBuff  : TNetBuffer;
    FFileName : String;
    FHnd: File of Byte;
    FBuffFull : boolean;
  public
    Constructor Create(Port: Word; Filename: String);
    Destructor Destroy; override;
    function SendRequest():boolean;
    procedure Execute; override;
  end;

var
  listenSock:TTCPListenThrd;
  recvSock:TTCPRecvThrd;
  sendSock:TTCPSendThrd;

implementation

uses DCPThread, CPgUnit5, CPgUnit6, RegExpr;

{ TTCPThrd }
procedure TTCPThrd.AddMsg2Q(Msg: String);
var l: longint;
begin
  l := Length(FMsgsQueue);
  SetLength(FMsgsQueue,l+1);
  FMsgsQueue[l] := Msg;
end;

procedure TTCPThrd.PrintError(const prefixStr: string='');
begin
  FErrorCode := SocketError;
  if FErrorCode <> SOCKET_ERROR then
  begin
    AddMsg2Q (prefixStr +'Socket error ('+IntToStr(FErrorCode)+'): '+SysErrorMessageUTF8(FErrorCode));
    PrintMsgsFromQ;
  end;
end;

Procedure TTCPThrd.PrintMsgsFromQ();
var l: integer;
begin
  if Self.Terminated then
    Exit;
  l := Length(FMsgsQueue);
  if l > 0 then
  begin
    Synchronize(@frmNetwork.ProcessSockMsgQueue);
    SetLength(FMsgsQueue,0);
  end;
end;

procedure TTCPThrd.setUIComponent(lbl: TLabel; pb:TProgressBar; lbx:TListBox; cbC: TComboBox; edH,edK:TEdit; btC:TButton);
begin
  btCancel := btC;
  lbFNTxt  := lbl;
  pbXfer   := pb;
  lbFNMsg  := lbx;
  cbCipher := cbC;
  edHost   := edH;
  edKey    := edK;
end;

function TTCPThrd.TryResolve(HostName: string;var ina:TInAddr):boolean;
var
  {$IFDEF UNIX}
  hst:THostEntry;
  {$ELSE IFDEF MSWINDOWS}
  hst: PHostEnt;
  inaddr: winsock2.TInAddr;
  {$ENDIF}
  re: TRegExpr;
begin
  Result := false;
  re := TRegExpr.Create;
  re.InputString := HostName;
  re.Expression := '^\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}$';
  if not re.Exec(re.InputString) then
  begin
{$IFDEF UNIX}
    if ResolveHostByName(HostName,hst) then
    begin
      ina := hst.Addr;
      Result := true;
    end else
      AddMsg2Q('Ne peut résoudre le nom!');
{$ELSE IFDEF MSWINDOWS}
    hst := gethostbyname(PChar(@HostName[1]));
    if hst <> nil then
    begin
      if hst^.h_addrtype = AF_INET then
      begin
        inaddr.s_addr :=
         UInt32(hst^.h_addr_list^[0])
        +(UInt32(hst^.h_addr_list^[1]) shl 8)
        +(UInt32(hst^.h_addr_list^[2]) shl 16)
        +(UInt32(hst^.h_addr_list^[3]) shl 24);
        ina.s_addr := inaddr.s_addr;
        Result := true;
      end;
    end else
      AddMsg2Q('Ne peut pas résoudre le nom.');
{$ENDIF}
  end else //else valid ip (1-254.0-254...) should be confirmed;
  begin
    ina := StrToNetAddr(HostName);
    Result := true;
  end;
end;

procedure TTCPThrd.waitForMs(ms:integer);
var tms,currTms,modulo: Comp;
  i: longint;
begin
  i := 0;
  tms := TimeStampToMSecs(DateTimeToTimeStamp(Now));
  currTms := tms;
  while tms + ms  > currTms do
  begin
    Synchronize(@Application.ProcessMessages);
    modulo := i mod 3;
    if modulo = 0 then
      if ms > 266 then
        Sleep(66)
      else Sleep(33);
    currTms := TimeStampToMSecs(DateTimeToTimeStamp(Now));
    Inc(i);
  end;
end;

{ TTCPCliThrd }
constructor TTCPCliThrd.Create(CreateSuspended: Boolean; const StackSize: SizeUInt=DefaultStackSize);
begin
  FMaxScrambleSz := 1020;
  FBufSz := SizeOf(TNetBuffer);
  FDtSz := SizeOf(TNetBufferData);
  FXFerCancel := false;
  FXFerDone := false;
  FClientSocket := LongInt(INVALID_HANDLE_VALUE);
  FScrambleSz := 0;
  FNeedSyncCh := false;
  FNeedSyncPb := false;
{*
  FFirstPacket := false;
  FLastPacket := false;
*}
  inherited Create(CreateSuspended,StackSize);
end;

procedure TTCPCliThrd.SetCancelXFer;
begin
  FXFerCancel := true;
end;

procedure TTCPCliThrd.UnsetCipher();
begin
  case TEncryption(FCIdx) of
    eBf:
      begin
        FDCPblowfish.Burn;
        FDCPblowfish.Free;
      end;
    eRdl:
      begin
        FDCPrijndael.Burn;
        FDCPrijndael.Free;
      end;
    e3des:
      begin
        FDCP3des.Burn;
        FDCP3des.Free;
      end;

    eRc6:
      begin
        FDCPrc6.Burn;
        FDCPrc6.Free;
      end;
    eSerpent:
      begin
        FDCPserpent.Burn;
        FDCPserpent.Free;
      end;
    eIce2:
      begin
        FDCPice2.Burn;
        FDCPice2.Free;
      end;
    eTwofish:
      begin
        FDCPtwofish.Burn;
        FDCPtwofish.Free;
      end;
    eCast256:
      begin
        FDCPcast256.Burn;
        FDCPcast256.Free;
      end;
  end;
end;

procedure TTCPCliThrd.SetCipher();
begin
  FKey:=edKey.Text;
  FCIdx:=cbCipher.ItemIndex;
  case TEncryption(FCIdx) of
    eBf      :
      begin
        FDCPblowfish := TDCP_blowfish.Create(nil,Self);
        FDCPblowfish.InitStr(FKey,TDCP_sha1);
      end;
    eRdl:
      begin
        FDCPrijndael := TDCP_rijndael.Create(nil,Self);
        FDCPrijndael.InitStr(FKey,TDCP_sha1);
      end;
    e3des:
      begin
        FDCP3des := TDCP_3des.Create(nil,Self);
        FDCP3des.InitStr(FKey,TDCP_sha1);
      end;

    eRc6:
      begin
        FDCPrc6 := TDCP_rc6.Create(nil,Self);
        FDCPrc6.InitStr(FKey,TDCP_sha1);
      end;
    eSerpent:
      begin
        FDCPserpent := TDCP_serpent.Create(nil,Self);
        FDCPserpent.InitStr(FKey,TDCP_sha1);
      end;
    eIce2:
      begin
        FDCPice2 := TDCP_ice2.Create(nil,Self);
        FDCPice2.InitStr(FKey,TDCP_sha1);
      end;
    eTwofish:
      begin
        FDCPtwofish := TDCP_twofish.Create(nil,Self);
        FDCPtwofish.InitStr(FKey,TDCP_sha1);
      end;
    eCast256:
      begin
        FDCPcast256 := TDCP_cast256.Create(nil,Self);
        FDCPcast256.InitStr(FKey,TDCP_sha1);
      end;
  end;

end;

function TTCPCliThrd.CryptBuffer(const BuffIn:TNetBuffer;bufSz:cardinal):TNetBuffer;
begin
  Result.tBuf := tbData;
  Result.dataLen := bufSz;
  Result.pckNum := BuffIn.pckNum;
  Result.firstPck := BuffIn.firstPck;
  Result.lastPck := BuffIn.lastPck;
  Result.crc32 := crc32(0,@BuffIn.datas[0],bufSz);
  case TEncryption(FCIdx) of
    eBf      :
      begin
        FDCPblowfish.EncryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
    eRdl:
      begin
        FDCPrijndael.EncryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
    e3des:
      begin
        FDCP3des.EncryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;

    eRc6:
      begin
        FDCPrc6.EncryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
    eSerpent:
      begin
        FDCPserpent.EncryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
    eIce2:
      begin
        FDCPice2.EncryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
    eTwofish:
      begin
        FDCPtwofish.EncryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
    eCast256:
      begin
        FDCPcast256.EncryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
  end;
end;

function TTCPCliThrd.DecryptBuffer(const buffIn:TNetBuffer;bufSz:cardinal): TNetBuffer;
begin
  FillChar(Result,SizeOf(Result),#0);
  case TEncryption(FCIdx) of
    eBf      :
      begin
        FDCPblowfish.DecryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
    eRdl:
      begin
        FDCPrijndael.DecryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
    e3des:
      begin
        FDCP3des.DecryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;

    eRc6:
      begin
        FDCPrc6.DecryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
    eSerpent:
      begin
        FDCPserpent.DecryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
    eIce2:
      begin
        FDCPice2.DecryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
    eTwofish:
      begin
        FDCPtwofish.DecryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
    eCast256:
      begin
        FDCPcast256.DecryptCBC(BuffIn.datas[0],Result.datas[0],bufSz);
      end;
  end;
  Result.tBuf := tbData;
  Result.pckNum := buffIn.pckNum;
  Result.firstPck := BuffIn.firstPck;
  Result.lastPck := BuffIn.lastPck;
  Result.dataLen := bufSz;
  Result.crc32 := crc32(0,@Result.datas[0],bufSz);
end;

function TTCPCliThrd.GetHeaderBuffer:TNetBuffer;
begin
  Result := FHdrBuff;
end;

procedure TTcpCliThrd.SetSockBuff(so: UInt32; bSz: UInt32);
begin
  if fpsetsockopt(FClientSocket,SOL_SOCKET,so,@bSz,SizeOf(bSz)) = SOCKET_ERROR then
    AddMsg2Q('['+Format('0x%0:p', [@Self])+'@TTcpCliThrd.SetSockBuff] SockOpt('+IntToStr(so)+','+IntToStr(bSz)+'): '+SysErrorMessage(socketerror));
end;

procedure TTCPCliThrd.SetSockRcvTmo(tmo: {$IFDEF UNIX}Timeval{$ELSE}Longint{$ENDIF});
begin
  if fpsetsockopt(FClientSocket,SOL_SOCKET,SO_RCVTIMEO,@tmo,SizeOf(tmo)) = SOCKET_ERROR then
    AddMsg2Q('['+Format('0x%0:p', [@Self])+'@TTCPCliThrd.SetSockRcvTmo] SendOpt('{$IFDEF UNIX}+'"'+IntToStr(tmo.tv_sec div 1000)+','+IntToStr(tmo.tv_usec * 1000)+'"'{$ELSE}+IntToStr(tmo){$ENDIF}+'): '+SysErrorMessage(socketerror));
end;

procedure TTCPCliThrd.SetSockSndTmo(tmo: {$IFDEF UNIX}Timeval{$ELSE}Longint{$ENDIF});
begin
  if fpsetsockopt(FClientSocket,SOL_SOCKET,SO_SNDTIMEO,@tmo,SizeOf(tmo)) = SOCKET_ERROR then
    AddMsg2Q('['+Format('0x%0:p', [@Self])+'@TTCPCliThrd.SetSockSndTmo] SendOpt('{$IFDEF UNIX}+'"'+IntToStr(tmo.tv_sec div 1000)+','+IntToStr(tmo.tv_usec * 1000)+'"'{$ELSE}+IntToStr(tmo){$ENDIF}+'): '+SysErrorMessage(socketerror));
end;

// Returns number of bytes waiting to read
function TTCPCliThrd.WaitingData : cardinal;
var
  avBytes : LongInt;
begin
  sleep(1);
  if {$IFDEF UNIX}FpIOCtl(FClientSocket, FIONREAD, @avBytes){$ELSE}WSAIOCtl(FClientSocket, FIONREAD, @avBytes,0,nil,0,nil,nil,nil){$ENDIF} <> 0 then
    Result := avBytes
  else
    Result := 0;
end;

{
Tests if data are available for reading within the timeout
@param Timeout Max time to wait until returns
@return True if data are available, false otherwise
}
function TTCPCliThrd.CanRead(Timeout : Integer) : Boolean;
var
  FDS: TFDSet;
  TimeV: TTimeVal;
begin
  {$IFDEF UNIX}fpFD_Zero{$ELSE}FD_ZERO{$ENDIF}(FDS);
  {$IFDEF UNIX}fpFD_SET{$ELSE}FD_SET{$ENDIF}(FClientSocket, FDS);
  TimeV.tv_usec := (Timeout mod 1000) * 1000;
  TimeV.tv_sec := Timeout div 1000;
  Result := {$IFDEF UNIX}fpSelect{$ELSE}Select{$ENDIF}(FClientSocket + 1, @FDS, nil, nil, @TimeV) > 0;
end;

{$IFNDEF WINDOWS}
function TTCPCliThrd.CanWrite(Timeout: integer): boolean;
var
  FDSet: TFDSet;
  TimeVal: PTimeVal;
  TimeV: TTimeVal;
  x: Integer;
begin
  TimeV.tv_usec := (Timeout mod 1000) * 1000;
  TimeV.tv_sec := Timeout div 1000;
  TimeVal := @TimeV;
  if Timeout = -1 then
    TimeVal := nil;
  {$IFDEF UNIX}
  fpFD_ZERO(FDSet);
  fpFD_SET(FClientSocket, FDSet);
  x := fpSelect(FClientSocket + 1, nil, @FDSet, nil, TimeVal);
  {$ELSE}
  FD_ZERO(FDSet);
  FD_SET(FHandle, FDSet);
  x := Select(FHandle + 1, nil, @FDSet, nil, TimeVal);
  {$ENDIF}
  if x <> SOCKET_ERROR then
    x := 0;
  Result := x > 0;
end;
{$ELSE}
function TTCPCliThrd.CanWrite(Timeout: integer): boolean;
var
  t: LongWord;
begin
{*
  Result := SendingData = 0;
  if not Result then
	  Result := CanEvent(EV_TXEMPTY, Timeout);
*}
  if Result and (Win32Platform <> VER_PLATFORM_WIN32_NT) then
  begin
    t := GetTickCount64;
{*
    while not ReadTxEmpty(FPortAddr) do
    begin
      if TickDelta(t, GetTickCount64) > 255 then
        Break;
      Sleep(1);
    end;
*}
    Sleep(1);
  end;
  Result := true;
{*
  if Result then
    DoStatus(HR_CanWrite, '');
*}
end;
{$ENDIF}

procedure TTCPCliThrd.UpdateProgress(Meth: string);
begin
  FNeedSyncPb := false;
  FNeedSyncCh := false;
  FPcProgress := Round((FCount/FHdrBuff.filesize)*100);
  if (pbXfer.Position <> Round((FCount/FHdrBuff.filesize)*100))
    and (FPcProgress < 100)
    and (FLastTickPb + 266 < GetTickCount64) then
    begin
      FNeedSyncPb := true;
      FLastTickPb := GetTickCount64;
    end;
  if (FLastTickCh + 999 < GetTickCount64) then
  begin
    FNeedSyncCh := true;
    FLastTickCh := GetTickCount64;
  end;
  if FNeedSyncPb or FNeedSyncCh then
  begin
    if Meth = 'Send' then
    begin
      Synchronize(@frmNetwork.UpdateProgressSend)
    end else
    begin
      Synchronize(@frmNetwork.UpdateProgressRecv);
    end;

    if FNeedSyncCh then
      FGraphRate := 0;
  end;
end;

{ TTCPListenThrd }

Constructor TTCPListenThrd.Create(Port: Word);
begin
  inherited Create(true);

  FPort := Port;
  FShutdown := false;
  FreeOnTerminate := true;

  inherited create(false);
end;

Destructor TTCPListenThrd.Destroy;
begin
  inherited Destroy;
end;

procedure TTCPListenThrd.Shutdown();
begin
  if FShutdown = false then
  begin
    if fpshutdown(FListenSocket,2) = SOCKET_ERROR then
      AddMsg2Q('['+Format('0x%0:p', [@listenSock])+'@TTCPListenThrd.Shutdown] fpShotdown('+IntToStr(FListenSocket)+',2): '+SysErrorMessageUTF8(socketerror))
    else
    begin
      AddMsg2Q('Fermeture de la conéxion en cours ...');
      FShutdown := true;
    end;
    waitForMs(333);
    PrintMsgsFromQ;
  end;
end;

procedure TTCPListenThrd.Execute;
var
  ClientSocket     : Longint;
  ClientAddr       : TInetSockAddr;
  ClientAddrSize   : LongInt;
begin
  try
    FServerAddr.sin_family := AF_INET;
    { port 4333 in network order }
    FServerAddr.sin_port := htons(FPort);

    if not TryResolve(edHost.Text,FServerAddr.sin_addr) then
    begin
      AddMsg2Q('['+Format('0x%0:p', [@listenSock])+'@TTCPListenThread.Execute] TryResolve: Cannot solve host name or IP address.');
      Exit;
    end;
    FListenSocket := fpsocket (AF_INET,SOCK_STREAM,0);
    if FListenSocket = SOCKET_ERROR Then
    begin
      PrintError('['+Format('0x%0:p', [@listenSock])+'@TTCPListenThrd.Execute] fpSocket ');
      Exit;
    end;
{$IFNDEF MSWINDOWS}
    if fpSetSockOpt(FListenSocket, SOL_SOCKET, SO_REUSEADDR, @FServerAddr, SizeOf(FServerAddr)) = SOCKET_ERROR then
    begin
      PrintError('['+Format('0x%0:p', [@listenSock])+'@TTCPListenThrd.Execute] fpSetSockOpt ');
      Exit;
    end;
{$ENDIF}
    if fpBind(FListenSocket,@FServerAddr,SizeOf(FServerAddr)) = SOCKET_ERROR then
    begin
      PrintError('['+Format('0x%0:p', [@listenSock])+'@TTCPListenThrd.Execute] fpBind ');
      Exit;
    end;
    if fpListen (FListenSocket,1) = SOCKET_ERROR then
    begin
      PrintError('['+Format('0x%0:p', [@listenSock])+'@TTCPListenThrd.Execute] fpListen ');
      Exit;
    end;

    ClientAddrSize := SizeOf(ClientAddr);
    repeat
      ClientSocket := fpAccept(ListenSocket,@ClientAddr,@ClientAddrSize);
      if ClientSocket = SOCKET_ERROR Then
      begin
        PrintError('['+Format('0x%0:p', [@listenSock])+'@TTCPListenThrd.Execute] fpAccept ');
        Break;
      end;

      recvSock := TTCPRecvThrd.Create(ClientSocket,ClientAddr);
      recvSock.setUIComponent(lbFNTxt,pbXfer,lbFNMsg,cbCipher,edHost,edKey, btCancel);
      recvSock.Start;

      ClientSocket := 0;
    until Terminated;
  finally
    AddMsg2Q('Sortie de thread listen!');
    PrintMsgsFromQ;
    Shutdown;
    Terminate;
  end;
end;

{ TTCPRecvThrd }

Constructor TTCPRecvThrd.Create(ClientSocket: longint; ClientAddr:TInetSockAddr);
begin
  inherited create(true);

  FClientSocket := ClientSocket;
  FClientAddr   := ClientAddr;
  Host          := NetAddrToStr(ClientAddr.sin_addr);

  FreeOnTerminate:=true;
end;

Destructor TTCPRecvThrd.Destroy;
begin
  inherited Destroy;
end;

function  TTCPRecvThrd.RecvRequest():boolean;
var availBytes: Boolean;
  nbs,rcnt: integer;
  fBuff: TNetBuffer;
begin
  try
    rcnt := 0;
    repeat
      nbs := fpRecv(FClientSocket,pByte(PtrUInt(@FHdrBuff) + rcnt),FBufSz,0);
      Inc(rcnt,nbs);
      if rcnt = FBufSz then
      begin
        if FHdrBuff.tBuf = tbHeader then
        begin
          AddMsg2Q('Requête de réception pour '+FHdrBuff.filename+' de '+IntToStr(FHdrBuff.filesize)+' octets.');
          Break;
        end else
        begin
          AddMsg2Q('['+Format('0x%0:p', [@RecvRequest])+'@TTCPRecvThrd.RecvRequest] Type de données invalide reçue!');
          FXFerCancel := true;
          Break;
        end;
      end else if (nbs <> SOCKET_ERROR) and (nbs > 0) then
      begin
        availBytes := CanRead(133);
        if not availBytes then
        begin
          AddMsg2Q('['+Format('0x%0:p', [@RecvRequest,IntToStr(nbs)])+'@TTCPRecvThrd.RecvRequest] Tampon fragmenté reçue (%so.)?!');
          FXFerCancel := true;
          Break;
        end;
      end else if nbs = SOCKET_ERROR then
      begin
        PrintError('['+Format('0x%0:p', [@RecvRequest])+'@TTCPRecvThrd.RecvRequest] fpRecv ');
        FXFerCancel := true;
        Break;
      end else
      begin
        AddMsg2Q('['+Format('0x%0:p', [@RecvRequest])+'@TTCPRecvThrd.RecvRequest] Réception nulle?!');
        FXFerCancel := true;
        Break;
      end;
    until availBytes;

    if FXFerCancel then
    begin
      Result := false;
      Exit;
    end;
    Synchronize(@frmNetwork.ProcessRecvRequest);

    FLastResp := 0;
    waitForAnswer := RTLEventCreate;
    while not Terminated do
    begin
      RTLEventWaitFor(waitForAnswer,30000);
      if frmQuestion.QAnswer > -1 then
        Break;
    end;
    RTLEventDestroy(waitForAnswer);

    fBuff.tBuf := tbResponse;
    if  frmQuestion.QAnswer <> 2 then
    begin
      FLastResp := 1;
      fBuff.value := FLastResp;
      nbs := fpSend(FClientSocket,@fBuff,FBufSz,0);
      Result := false;
      AddMsg2Q('Réception refusé!');
      FXFerCancel := true;
      Exit;
    end else
      FLastResp := 2;

    fBuff.value := FLastResp;
    nbs := fpSend(FClientSocket,@fBuff,FBufSz,0);

    FFileName := FHdrBuff.filename;

    Result := true;

    except on e: Exception do
    begin
      AddMsg2Q(e.Message);
      PrintMsgsFromQ;
      FXFerCancel := true;
      Result := false;
    end;
  end;
end;


procedure TTCPRecvThrd.Execute;
var
  availBytes: Boolean;
  i,wcnt,rcnt,cnt,zeroloop: LongInt;
  crc32buffer: Cardinal;
  tmo: {$IFDEF UNIX}Timeval{$ELSE}Longint{$ENDIF};
begin
  try
    try
      wcnt := 0;
      cnt := 0;
      rcnt := 0;
      availBytes := false;
  {$IFDEF UNIX}
      tmo.tv_sec := 30;
      tmo.tv_usec := 0;
  {$ELSE IFDEF MSWINDOWS}
      tmo := 30000;
  {$ENDIF}
      FPrvPckNum := 0;
      FXFerCancel := false;
      FXFerDone := false;
      SetSockRcvTmo(tmo);
      //Read Header
      waitForMs(133);
      if not RecvRequest() then
      begin
        FXFerCancel := true;
        fpshutdown(FClientSocket,2);
        PrintMsgsFromQ;
        Exit;
      end else AddMsg2Q('Début de réception!');
      PrintMsgsFromQ;
  {$IFDEF UNIX}
      tmo.tv_sec := 60;
      tmo.tv_usec := 0;
  {$ELSE IFDEF MSWINDOWS}
      tmo := 7000;
  {$ENDIF}
      SetSockBuff(SO_RCVBUF,FBufSz * 32);
      SetSockBuff(SO_RCVLOWAT, FBufSz);

      SetSockRcvTmo(tmo);
      SetSockSndTmo(tmo);

      //Open file in rewrite mode
      Assign(FHnd,conf.AppConfig.downloadPath + DirectorySeparator + FHdrBuff.filename);
      Rewrite(FHnd);

      SetCipher();
      FLastTickPb := 0;
      FLastTickCh := 0;

      Synchronize(@frmNetwork.DoBtCancelVisible);

      // Handling receiption of data record
      if not FXFerCancel then
      begin
        cnt := 0;
        zeroloop := 0;
        waitForMs(133);
        repeat
          rcnt := fprecv(FClientSocket,pByte(PtrUInt(@FRcvBuff) + cnt),FBufSz,0);
          if rcnt = SOCKET_ERROR then
          begin
            FErrorCode := SocketError();
            if (FErrorCode <> EsockENOBUFS) and (FErrorCode <> EsockEWOULDBLOCK) then
            begin
              PrintError('['+Format('0x%0:p', [@TTCPRecvThrd.Execute])+'@TTCPRecvThrd.Execute] fpRecv: ');
              FXFerCancel := true;
              Exit;
            end;
          end else FErrorCode := 0;

          Inc(cnt, rcnt);
          if (cnt < FBufSz) then
          begin
            repeat
{$IFDEF DEBUG}
              AddMsg2Q('Incomplete buffer received!');
{$ENDIF}
              if (zeroloop = 7) then
              begin
                FXFerCancel := true;
                AddMsg2Q('Impossible de compléter le tampon, délai écoulé!');
                Break;
              end else
              begin
                availBytes := CanRead(133);
                //(int)availBytes := WaitingData;
                if not availBytes then
                begin
                  Inc(zeroloop);
                  Continue;
                end;
              end;
            until availBytes;
          end else if (cnt = FBufSz) then
          begin
            if FRcvBuff.tBuf <> tbData then
            begin
              AddMsg2Q('['+Format('0x%0:p@TTCPRecvThrd.Execute] fpRecv: Mauvais type de données (%.02d) transmises.', [@TTCPRecvThrd.Execute,FRcvBuff.tBuf]));
              FXFerCancel := true;
              Exit;
            end;
            if FPrvPckNum = FRcvBuff.pckNum then
            begin
              AddMsg2Q('['+Format('0x%0:p@TTCPRecvThrd.Execute] fpRecv: Numéro de paquet identique (%s), retransmission/relecture?!', [@TTCPRecvThrd.Execute,FRcvBuff.pckNum]));
              FXFerCancel := true;
              Exit;
            end;
{$IFDEF DEBUG}
            AddMsg2Q('Packet number (current/previous): '
              + IntToStr(FRcvBuff.pckNum) + '/' + IntToStr(FPrvPckNum));
            AddMsg2Q('Packet CRC32 : ' + IntToHex(FRcvBuff.crc32));
            AddMsg2Q('Data length : ' + IntToStr(FRcvBuff.dataLen));
{$ENDIF}
{* Decrypt 1 data block of FRcvBuff *}
            crc32buffer := FRcvBuff.crc32;
            FRcvBuff := DecryptBuffer(FRcvBuff,FRcvBuff.dataLen);
            if crc32buffer <> FRcvBuff.crc32 then
              AddMsg2Q('['+Format('0x%0:p', [@TTCPRecvThrd.Execute])+'@TTCPRecvThread.Execute] Erreur CRC32 ('+Format('[%0:8.8x]',[crc32buffer])+' <> '+Format('[%0:8.8x]',[FRcvBuff.crc32])+') bloc ['+IntToStr(FRcvBuff.pckNum)+'].');
            FPrvPckNum := FRcvBuff.pckNum;
          end;

          if FRcvBuff.firstPck then
          begin
            //Determine size for first scramble
            SetLength(FScramble,FMaxScrambleSz);
            for i := 0 to FDtSz - 1 do
            begin
              FScramble[i] := FRcvBuff.datas[i];
              //Find mark of scramble end
              if FScramble[i] = 0 then
              begin
                FScrambleSz := i + 1;
                Break;
              end;
            end;
            //Adjust scramble size array
            SetLength(FScramble, FScrambleSz);
            //Set real file size
            Dec(FHdrBuff.filesize,FScrambleSz * 2);
{$IFDEF DEBUG}
            AddMsg2Q('First data packet (ScrambleSz=' + IntToStr(FScrambleSz) + ', Real FileSz=' + IntToStr(FHdrBuff.filesize) + ')');
{$ENDIF}
          end;
{* Remove scramble from first buffer *}
          if FRcvBuff.firstPck then
          begin
            for i := FScrambleSz to FRcvBuff.dataLen - 1 do
              FRcvBuff.datas[i - FScrambleSz] := FRcvBuff.datas[i];
            Dec(FRcvBuff.dataLen, FScrambleSz);
            FRcvBuff.firstPck := false;
          end;
          Inc(FCount,FRcvBuff.dataLen);
          Inc(FGraphRate, cnt);
          BlockWrite(FHnd,FRcvBuff.datas[0],FRcvBuff.dataLen,wcnt);
          UpdateProgress('Recv');
          if (FRcvBuff.lastPck = true) then
          begin
            FileFlush(FileRec(FHnd).Handle);
            FileTruncate(FileRec(FHnd).Handle,FHdrBuff.filesize);
            FRcvBuff.lastPck := false;
            FXFerDone := true;
          end;
          cnt := 0;
        until FXFerDone or FXFerCancel;

        if FXFerCancel then
          Exit;
      end;
      PrintMsgsFromQ;

      if FHdrBuff.filesize = FCount then
      begin
        FXFerDone := true;
        AddMsg2Q('Réception terminé.');
      end else if FXFerCancel then
        AddMsg2Q('Transfert annulé par l''utilisateur, connexion fermé.'
          +'Quantité reçu/attendu:'+IntToStr(FCount)+'/'+IntToStr(FHdrBuff.filesize)+' octets.')
      else
        AddMsg2Q('Réception incorrect.'+#13#10
          +'Quantité reçu/attendu:'+IntToStr(FCount)+'/'+IntToStr(FHdrBuff.filesize)+' octets.');

      Synchronize(@frmNetwork.ResetRecvUI);
      if FXFerDone then
        repeat
          wcnt := fpsend(FClientSocket,PByte(1),1,0);
        until (wcnt = SOCKET_ERROR) or (wcnt >= 1);
    except on e: Exception do
      AddMsg2Q(e.Message);
    end;

  finally
    if FLastResp = 2 then
    begin
      UnsetCipher();
      Close(FHnd);
    end;
    waitForMs(133);
    if fpshutdown(FClientSocket,2) <> 0 then
    begin
      waitForMs(333);
      CloseSocket(FClientSocket);
    end;
    AddMsg2Q('Conéxion fermé!');
    Synchronize(@frmNetwork.ResetRecvUI);
    PrintMsgsFromQ;
    Terminate;
  end;
end;

{ TTCPSendThrd }

Constructor TTCPSendThrd.Create(Port: Word; Filename: String);
begin
  inherited Create(true);

  OnTerminate:=@frmNetwork.SendTerminate;

  FFileName := Filename;
  FPort := Port;
  FXFerDone := false;

  FreeOnTerminate:=true;
end;

Destructor TTCPSendThrd.Destroy;
begin
  inherited Destroy;
end;

function TTCPSendThrd.SendRequest():boolean;
var i, cnt: Integer;
  resp:TNetBuffer;
  FlHnd: THandle;
begin
  Result := false;
  try
    try
      AddMsg2Q('Envoi de la requête d''autorisation ...');
      PrintMsgsFromQ;
      FillByte(FHdrBuff, FBufSz, 0);
      FHdrBuff.pckNum := FPckNum;
      FHdrBuff.tBuf := tbHeader;
      FHdrBuff.filename := ExtractFileName(FFileName);
      flHnd := FileOpen(FFileName, fmShareDenyNone);
      if FlHnd <> INVALID_HANDLE_VALUE then
      begin
        FHdrBuff.filesize := FileSeek(flHnd,0,soFromEnd) + (FScrambleSz*2);
        FileClose(flHnd);
      end else
        raise Exception.Create('['+Format('0x%0:p', [@TTCPSendThrd.SendRequest])+'@TTCPSendThrd.Execute] File "'+FHdrBuff.filename+'" not found.');

      i := 1;
      while not CanWrite(67) do
      begin
        if i = 3 then Break;
        Inc(i);
      end;

      cnt := fpSend(FClientSocket,@FHdrBuff,FBufSz,0);
      if cnt = SOCKET_ERROR then
        raise Exception.Create('['+Format('0x%0:p',[@TTCPSendThrd.SendRequest])+'@TTCPSendThrd.SendRequest] fpSend : '+SysErrorMessageUTF8(socketerror));

      Inc(FPckNum);
      AddMsg2Q('Attente de la réponse ...');
      PrintMsgsFromQ;
      cnt := fpRecv(FClientSocket,@resp,FBufSz,0);
      if cnt = SOCKET_ERROR then
        raise Exception.Create('['+Format('0x%0:p',[@TTCPSendThrd.SendRequest])+'@TTCPSendThrd.SendRequest] fpRecv : '+SysErrorMessageUTF8(socketerror));

      if resp.tBuf = tbResponse then
      begin
        if resp.value = 2 then
          Result := true
        else
        begin
          FXFerCancel := true;
          Result := false;
          Exit;
        end;
      end else
        raise Exception.Create('['+Format('0x%0:p',[@TTCPSendThrd.SendRequest])+'@TTCPSendThrd.SendRequest] fpRecv : Invalid buffer type.');

      Synchronize(@frmNetwork.ProcessSendRequest);

      except on e: Exception do
      begin
        AddMsg2Q(e.Message);
        FXFerCancel := true;
        Result := false;
      end;
    end;
  finally
    PrintMsgsFromQ;
  end;
end;

procedure TTCPSendThrd.Execute;
var i,errorcode,zerocnt,cnt,rcnt,lastpacketsz: integer;
  tmo: {$IFDEF UNIX}Timeval{$ELSE}Longint{$ENDIF};
begin
  try
    try
      FPckNum := 1;
      rcnt := 0;
      cnt := 0;
      FCount := 0;
      zerocnt:=0;
      FPcProgress := 0;
      FXFerDone := false;
      FXFerCancel := false;

      FClientAddr.sin_family := AF_INET;
{* port 4333 in network order *}
      FClientAddr.sin_port := htons(FPort);
{* Solve name to ip v4? *}
      if not TryResolve(edHost.Text,FClientAddr.sin_addr) then
      begin
        FXFerCancel := true;
        raise Exception.Create('['+Format('0x%0:p',[@TTCPSendThrd.Execute])+'@TTCPSendThrd.Execute] Cannot solve hostname "'+edHost.Text+'"');
      end;

      AddMsg2Q('Connéxion à '+NetAddrToStr(FClientAddr.sin_addr));

      FClientSocket := fpSocket(AF_INET,SOCK_STREAM,0);
      if FClientSocket = SOCKET_ERROR Then
      begin
        PrintError('['+Format('0x%0:p',[@TTCPSendThrd.Execute])+'@TTCPSendThrd.Execute] fpSocket ');
        FXFerCancel := true;
      end;
      if not FXFerCancel then
      begin
        if (fpConnect(FClientSocket,@FClientAddr,SizeOf(FClientAddr)) = SOCKET_ERROR) Then
          raise Exception.Create('['+Format('0x%0:p',[@TTCPSendThrd.Execute])+'@TTCPSendThrd.Execute] '+SysErrorMessageUTF8(SocketError));
      end;
{$IFDEF LINUX}
      tmo.tv_sec := 60;
      tmo.tv_usec := 0;
{$ELSE IFDEF MSWINDOWS}
      tmo := 60000;
{$ENDIF}

      SetSockBuff(SO_SNDBUF,FBufSz * 32);
//      SetSockBuff(SO_SNDLOWAT, FBufSz);
      SetSockRcvTmo(tmo);
      SetSockSndTmo(tmo);

      if not FXFerCancel then
      begin
{* Define scramble size *}
        repeat
          FScrambleSz := Random(1024);
        until (FScrambleSz > 512) and (FScrambleSz < FMaxScrambleSz);

{* Send request to remote socket *}
        if not SendRequest() then
        begin
          AddMsg2Q('Envoi refusé.');
          Exit;
        end else AddMsg2Q('Envoi accepté.');
        PrintMsgsFromQ;
      end else
      begin
        AddMsg2Q('Envoi refusé automatiquement.');
        Exit;
      end;

{$IFDEF UNIX}
      tmo.tv_sec := 7;
      tmo.tv_usec := 0;
{$ELSE IFDEF MSWINDOWS}
      tmo := 7000;
{$ENDIF}
      SetSockRcvTmo(tmo);
      SetSockSndTmo(tmo);

      FLastTickPb := 0;
      FLastTickCh := 0;
      FBuffFull := false;

      SetCipher();
      Assign(fHnd,FFileName);
      Reset(fHnd);

      {* Set scramble from scramble size defined before sending request *}
      SetLength(FScramble,FScrambleSz);
      for i := 0 to FScrambleSz - 1 do
        repeat
          FScramble[i] := Random(255);
        until FScramble[i] > 0;
      FScramble[FScrambleSz - 1] := 0;

      FillByte(FSndBuff, FBufSz, 0);
      FSndBuff.tBuf := tbData;
      FSndBuff.firstPck := true;

      while not FXFerDone and not FXFerCancel and not EOF(fHnd) do
      begin
        FErrorCode := 0;
        FSndBuff.pckNum := FPckNum;
        FSndBuff.tBuf := tbData;
        if not FBuffFull then
        begin
          if FSndBuff.firstPck then
          begin
            for i := 0 to FScrambleSz - 1 do
              FSndBuff.datas[i] := FScramble[i];

            BlockRead(FHnd, FSndBuff.datas[FScrambleSz], FDtSz - FScrambleSz, rcnt);
            Inc(rcnt, FScrambleSz);
          end else
            BlockRead(FHnd, FSndBuff.datas[0], FDtSz, rcnt);
{* need at least 1 data block *}
          if EOF(FHnd) then
          begin
            for i := rcnt to FDtSz - 1 do
            begin
              FSndBuff.datas[i] := FScramble[i - rcnt];
              if FScramble[i - rcnt] = 0 then
              begin
                FSndBuff.lastPck := true;
                Break;
              end;
            end;
            FSndBuff.dataLen := i;
            lastpacketsz := i - rcnt;
            FSndBuff := CryptBuffer(FSndBuff,FSndBuff.dataLen);
            cnt := fpSend(FClientSocket,@FSndBuff,FBufSz,0);
{$IFDEF DEBUG}
            AddMsg2Q('Packet number: ' + IntToStr(FSndBuff.pckNum));
            AddMsg2Q('Packet CRC32 : ' + IntToHex(FSndBuff.crc32));
            AddMsg2Q('Data length/sent : ' + IntToStr(FSndBuff.dataLen)+ '/' + IntToStr(cnt));
{$ENDIF}
            if cnt <> SOCKET_ERROR then
            begin
              Inc(FPckNum);
              Inc(FGraphRate,cnt);
              Inc(FCount, FSndBuff.dataLen - FScrambleSz - (FScrambleSz - lastpacketsz));
              UpdateProgress('Send');
{* need 2 data block *}
              if not FSndBuff.lastPck then
              begin
                waitForMs(99);
                FSndBuff.tBuf := tbData;
                FSndBuff.pckNum := FPckNum;
                FSndBuff.lastPck := true;
                for i := lastpacketsz to FScrambleSz - 1 do
                begin
                  FSndBuff.datas[i - lastpacketsz] := FScramble[i];
                  if FScramble[i] = 0 then
                    Break;
                end;
                FSndBuff.dataLen := i;
                FSndBuff := CryptBuffer(FSndBuff,FSndBuff.dataLen);
                cnt := fpSend(FClientSocket,@FSndBuff,FBufSz,0);
{$IFDEF DEBUG}
                AddMsg2Q('Small file detected.');
                AddMsg2Q('Packet number: ' + IntToStr(FSndBuff.pckNum));
                AddMsg2Q('Packet CRC32 : ' + IntToHex(FSndBuff.crc32));
                AddMsg2Q('Data length/sent : ' + IntToStr(FSndBuff.dataLen)+ '/' + IntToStr(cnt));
{$ENDIF}
                if cnt <> SOCKET_ERROR then
                begin
                  Inc(FPckNum);
                  Inc(FGraphRate,cnt);
                  Inc(FCount,FSndBuff.dataLen);
                  UpdateProgress('Send');
                end else
                  raise Exception.Create('['+Format('0x%0:p@TTCPSendThrd.Execute] fpSend last data packet(%s).',[@TTCPSendThrd.Execute,cnt]));
                FXFerDone := true;
              end;
              if FSndBuff.firstPck then FSndBuff.firstPck := false;
              if FSndBuff.lastPck then FSndBuff.lastPck := false;
              FXFerDone := true;
              Continue;
            end else
              raise Exception.Create('['+Format('0x%0:p',[@TTCPSendThrd.Execute])+'@TTCPSendThrd.Execute] fpSend last data packet');
          end; {* if FEOF(fHnd) ... *}
          FSndBuff.pckNum := FPckNum;
          FSndBuff.dataLen := rcnt;
          FSndBuff := CryptBuffer(FSndBuff,FSndBuff.dataLen);
{$IFDEF DEBUG}
          AddMsg2Q('Packet number: ' + IntToStr(FSndBuff.pckNum));
          AddMsg2Q('Packet CRC32 : ' + IntToHex(FSndBuff.crc32));
          AddMsg2Q('Data length : ' + IntToStr(FSndBuff.dataLen));
{$ENDIF}
          cnt := fpSend(FClientSocket,@FSndBuff,FBufSz,0);
          if cnt <> SOCKET_ERROR then
          begin
            FErrorCode := 0;
            Inc(FPckNum);
            Inc(FGraphRate,cnt);
            Inc(FCount,FSndBuff.dataLen);
            UpdateProgress('Send');
            if FSndBuff.firstPck then FSndBuff.firstPck := false;
            if FSndBuff.lastPck then FSndBuff.lastPck := false;
          end else
            FErrorCode := errorcode; {* if cnt > 0 & cnt < FDtSz & not feof(FHnd) *}
          if (FErrorCode = EsockENOBUFS) or (FErrorCode = EsockEWOULDBLOCK) then
          begin
{$IFDEF DEBUG}
            AddMsg2Q('Incomplete buffer sent!');
{$ENDIF}
            repeat
              if zerocnt > 10 then
              begin
                zerocnt := 0;
                FXFerCancel := true;
                FBuffFull := true;
                Break;
              end;
              Inc(zerocnt);
            until CanWrite(33);
{*
            if not FBuffFull then
            begin
              // can't have any offset so retransmits ???
              //foffset := cnt;
              //cnt := fpSend(FClientSocket,pByte(PtrUInt(@FSndBuff) + foffset),FBufSz - foffset,0);
              cnt := fpSend(FClientSocket,@FSndBuff,FBufSz,0);
            end else
              raise Exception.Create('['+Format('0x%0:p',[@TTCPSendThrd.Execute])+'@TTCPSendThrd.Execute] fpSend: '+SysErrorMessageUTF8(FErrorCode));
*}
          end; {* if FErrorCode <> ESock... *}
        end; {* not FBuffFull... *}
      end; {* while not FXFer... *}

      if FXFerDone then
        AddMsg2Q('Envoi complété.')
      else if FXFerCancel then
        AddMsg2Q('Transfert annulé.');

{$IFDEF UNIX}
      tmo.tv_sec := 30;
      tmo.tv_usec := 0;
{$ELSE IFDEF MSWINDOWS}
      tmo := 30000;
{$ENDIF}
      SetSockRcvTmo(tmo);
      SetSockSndTmo(tmo);

      FillByte(FSndBuff,FBufSz,0);
      FSndBuff.pckNum := FPckNum;
      FSndBuff.tBuf := tbClose;
      FSndBuff.value := 2;
//      cnt := fprecv(FClientSocket,@FSndBuff,FBufSz,0);
      except on e: Exception do
      begin
        AddMsg2Q(e.Message);
        Synchronize(@frmNetwork.ResetSendUI);
        PrintMsgsFromQ;
      end;
    end;
  finally
    UnsetCipher();
    FileFlush(TFileRec(FHnd).Handle);
    Close(fHnd);

    if (fpshutdown(FClientSocket,2) <> 0) then
    begin
      waitForMs(233);
      if (CloseSocket(FClientSocket) = 0) then
        AddMsg2Q('Conéxion fermé.');
    end;
    PrintMsgsFromQ;
    Synchronize(@frmNetwork.ResetSendUI);
  end;
  Terminate;
end;

initialization
  listenSock := nil;
  sendSock := nil;
  recvSock := nil;
end.
