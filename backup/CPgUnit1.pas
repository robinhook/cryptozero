unit CPgUnit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LazFileUtils, LResources, LCLType,
{$ifdef MSWINDOWS}
  Windows,
{$endif}
  Forms,
  Controls, Graphics, Dialogs, StdCtrls, Menus, ComCtrls, ExtCtrls,
{$ifdef MSWINDOWS}
  UDragDropMemo, ActiveX,
{$endif}DCPrijndael, DCPdes, DCPsha1, DCPblowfish, DCPserpent,
DCPtwofish, DCPrc6, DCPcast256, DCPice, DCPThread, AppCfgUnit, LazUTF8;


type
  { TfrmMain }

  TfrmMain = class(TForm)
    btCancelFile: TButton;
    btCancelAll: TButton;
    lbNbFiles: TLabel;
    lbFileSize: TLabel;
    MainMenu1: TMainMenu;
    MenuItem5: TMenuItem;
    MenuItem9: TMenuItem;
    mmStr: TMemo;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    OpenDialog1: TOpenDialog;
    pbMultiFiles: TProgressBar;
    StatusBar1: TStatusBar;
    GroupBox1: TGroupBox;
    lbFName: TLabel;
    GroupBox5: TGroupBox;
    btRAZStr: TButton;
    btCryptStr: TButton;
    btCryptFile: TButton;
    btDecryptFile: TButton;
    btDecryptStr: TButton;
    btOpenFile: TButton;
    lbError: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    cbxCipher: TComboBox;
    edKey: TEdit;
    btGenKey: TButton;
    Label2: TLabel;
    cbxCipher2: TComboBox;
    edKey2: TEdit;
    Label4: TLabel;
    btGenKey2: TButton;
    pbFile: TProgressBar;
    tmScanFiles: TTimer;
    procedure btCancelAllClick(Sender: TObject);
    procedure btCancelFileClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure cbxCipher2Change(Sender: TObject);
    procedure cbxCipherChange(Sender: TObject);
    procedure edKey2Change(Sender: TObject);
    procedure edKeyChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure OpenFileClick(Sender: TObject);
    procedure CryptFileClick(Sender: TObject);
    procedure DecryptFileClick(Sender: TObject);
    procedure Quitter1Click(Sender: TObject);
    procedure Fermer1Click(Sender: TObject);
    procedure btRAZStrClick(Sender: TObject);
    procedure CryptStrClick(Sender: TObject);
    procedure DecryptStrClick(Sender: TObject);
    procedure closeCancelClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure mmStrChange(Sender: TObject);
    procedure btGenKeyClick(Sender: TObject);
    procedure btGenKey2Click(Sender: TObject);
    procedure tmScanFilesTimer(Sender: TObject);
  private
    { Déclarations privées }
    PosChPrg: byte;
    AbortAllOps,AbortOp:boolean;
    DCP_blowfish : TDCP_Blowfish;
    DCP_3des: TDCP_3des;
    DCP_rijndael : TDCP_Rijndael;
    DCP_twofish : TDCP_twofish;
    DCP_rc6 : TDCP_rc6;
    DCP_cast256 : TDCP_cast256;
    DCP_serpent : TDCP_serpent;
    DCP_ice2 : TDCP_ice2;
    function RecurScanFiles(FileName: string; var errLst:TStringList):boolean;
  protected
    { Déclarations protégées }
  public
    { Déclarations publiques }
    Key: string;
    Col, Row:LongInt;
    Cipher,curIdx: integer;
    lastPos,curSz,totSz: int64;
    ImgIn,ImgOut:TMemoryStream;
    ApplicationPropertiesDropFiles: TApplicationProperties;
    procedure ApplicationPropertiesDropFilesAction(Sender: TObject;
      const FileNames: array of String);
    procedure ResetFileGroupBox();
  end;

var
  frmMain: TfrmMain;

implementation

uses Math,CPgUnit2,CPgUnit3,CPgUnit4,CpgUnit5;

function GenKey(szKey:integer): string;
var i,l: integer;
begin
  result := '';
  l := length(basekey);
  for i:=1 to szKey div 8 do
  begin
    result := result + basekey[floor(Random*l)];
    Sleep(i+(Byte(result[i]) shr 4));
  end;
end;

procedure ProgressFile(pos,taille: cardinal);
begin
//  ShowMessage('Progression '+IntToStr(position)+' / '+IntToStr(taille)+' / '+IntToStr(Form4.totSz));
  if (pos > 0) and (taille > 0) then
  begin
    if pos < frmMain.lastPos then
      frmMain.lastPos := 0;
    frmMain.pbFile.Position := Round((pos / taille)*100);
    frmMain.curSz := frmMain.curSz + (pos - frmMain.lastPos);
    frmMain.lastPos := pos;
    if((frmMain.curSz > 0) and (frmMain.totSz > 0)) then
      frmMain.pbMultiFiles.Position := Round((frmMain.curSz / frmMain.totSz)*100);
  end else
  begin
    frmMain.pbFile.Position := 0;
    frmMain.lastPos := 0;
  end;
  frmMain.Repaint;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var i: integer;
begin
  PosChPrg := 6;
  StatusBar1.SimplePanel := true;
  lbFName.Caption := 'Aucun';
  mmStr.Text := '';
  pbFile.Visible := false;

  if conf.AppConfig.saveKey = true then
  begin
    cbxCipher.ItemIndex := integer(conf.AppConfig.lastCipherStrm);
    edKey.Text := conf.AppConfig.lastKeyStrm;

    cbxCipher2.ItemIndex := integer(conf.AppConfig.lastCipherStr);
    edKey2.Text := conf.AppConfig.lastKeyStr;
  end else
  begin
    cbxCipher.ItemIndex := integer(eBf);
    edKey.Text := '';

    cbxCipher2.ItemIndex := integer(eBf);
    edKey2.Text := '';
  end;

  if (conf.AppConfig.formstate[1].top > 0) and (conf.AppConfig.formstate[1].left > 0) then
  begin
    Top := conf.AppConfig.formstate[1].top;
    Left := conf.AppConfig.formstate[1].left;
  end;
  if (conf.AppConfig.formstate[1].width > 0) and (conf.AppConfig.formstate[1].height > 0) then
  begin
    Width := conf.AppConfig.formstate[1].width;
    Height := conf.AppConfig.formstate[1].height;
  end;

  ApplicationPropertiesDropFiles := TApplicationProperties.Create(frmMain);
  ApplicationPropertiesDropFiles.OnDropFiles := @ApplicationPropertiesDropFilesAction;

  btCryptFile.Enabled := false;
  btDecryptFile.Enabled := false;
  btCryptStr.Enabled := false;
  btDecryptStr.Enabled := false;
  btCancelAll.Visible := false;
  btCancelFile.Visible := false;


  btOpenFile.Enabled := true;
  lbFName.Caption := 'Aucun';
  totSz := 0;
  pbFile.Position:=0;
  pbMultiFiles.Position:=0;
  curSz:=0;
  curIdx:=0;
  lastPos:=0;
  if conf.ListFile.Count > 0 then
  begin
    btCryptFile.Enabled := true;
    btDecryptFile.Enabled := true;
    btOpenFile.Enabled := true;

    StatusBar1.SimpleText := 'Prêt à crypter/décrypter!';
    lbFName.Caption := conf.ListFile[0];
    lbNbFiles.Caption:='Fichier 1 de '+IntToStr(conf.ListFile.Count);
    lbFileSize.Caption:='Taille : '+IntToStr(ceil(FileSize(conf.ListFile[0])/1024))+' ko.';

    for i := 0 to conf.ListFile.Count - 1 do
      if (conf.ListFile[i] <> '') and FileExists(conf.ListFile[i]) then
      begin
        Inc(totSz,FileSize(conf.ListFile[i]));
      end else
      begin
        btCryptFile.Enabled := false;
        btDecryptFile.Enabled := false;
        StatusBar1.SimpleText := 'Erreur le fichier "'+conf.ListFile[i]+'" n''est pas valide!';
        break;
      end;
  end;
end;

procedure TfrmMain.FormHide(Sender: TObject);
begin
  conf.AppConfig.formstate[1].top := Top;
  conf.AppConfig.formstate[1].left := Left;
  conf.AppConfig.formstate[1].width := Width;
  conf.AppConfig.formstate[1].height := Height;
end;

procedure TfrmMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  conf.AppConfig.formstate[1].top := Top;
  conf.AppConfig.formstate[1].left := Left;
  conf.AppConfig.formstate[1].width := Width;
  conf.AppConfig.formstate[1].height := Height;
  {$ifdef MSWINDOWS}
  RevokeDragDrop(Handle);
  {$endif}
end;

procedure TfrmMain.edKey2Change(Sender: TObject);
begin
  conf.AppConfig.lastKeyStr := edKey2.Text;
end;

procedure TfrmMain.edKeyChange(Sender: TObject);
begin
  conf.AppConfig.lastKeyStrm := edKey.Text;
end;

procedure TfrmMain.cbxCipher2Change(Sender: TObject);
begin
  conf.AppConfig.lastCipherStr := TEncryption(cbxCipher2.ItemIndex);
end;

procedure TfrmMain.Button2Click(Sender: TObject);
begin

end;

procedure TfrmMain.btCancelAllClick(Sender: TObject);
begin
  AbortAllOps := true;
  AbortOp := true;
end;

procedure TfrmMain.btCancelFileClick(Sender: TObject);
begin
  AbortOp := true;
end;

procedure TfrmMain.cbxCipherChange(Sender: TObject);
begin
  conf.AppConfig.lastCipherStrm := TEncryption(cbxCipher.ItemIndex);
end;

procedure TfrmMain.ResetFileGroupBox();
begin
  lbFName.Caption := 'Aucun';
  lbNbFiles.Caption := 'Fichier 0 de 0';
  lbFileSize.Caption := 'Taille : 0 octets';
end;

procedure TfrmMain.MenuItem1Click(Sender: TObject);
begin
  frmConfigure.Visible := true;
  frmMain.Enabled := false;
end;

procedure TfrmMain.MenuItem2Click(Sender: TObject);
begin
  frmHisto.Visible := true;
end;

procedure TfrmMain.MenuItem3Click(Sender: TObject);
begin
  ShowMessage('CryptoZero v'+conf.GetVersion());
end;

procedure TfrmMain.MenuItem4Click(Sender: TObject);
begin
   frmQueue.Visible := true;
end;

procedure TfrmMain.MenuItem5Click(Sender: TObject);
begin
  frmNetwork.Visible := true;
end;

function TfrmMain.RecurScanFiles(FileName: string; var errLst:TStringList):boolean;
var ffiles,fdirs:array of string;
  ff:TSearchRec;
  i,fc,dc:integer;
begin
  result := false;
  if FindFirst(FileName+DirectorySeparator+'*',faAnyFile,ff) = 0 then
  begin
    fc := 0;
    dc := 0;
    repeat
      if {$IFDEF LINUX}((ff.Attr and faSymLink) <> faSymLink) and{$ENDIF}
        ((ff.Attr and faDirectory) <> faDirectory) then
      begin
        SetLength(ffiles,fc+1);
        ffiles[fc] := FileName + DirectorySeparator + ff.Name;
        totSz := totSz + ff.Size;
        Inc(fc);
      end else if (ff.Name <> '.') and (ff.Name <> '..')
        {$IFDEF LINUX}and ((ff.Attr and faSymLink) <> faSymLink){$ENDIF} then
      begin
        if FileIsWritable(FileName + DirectorySeparator + ff.Name) then
        begin
          SetLength(fdirs,dc+1);
          fdirs[dc] := FileName + DirectorySeparator + ff.Name;
          Inc(dc);
        end else
          errLst.Add('Le dossier "'+FileName + DirectorySeparator + ff.Name+'" n''est pas accessible en écriture!');
      end else if (ff.Name <> '.') and (ff.Name <> '..') then
      begin
        errLst.Add('Le fichier '+ff.Name+' n''a pu être ajouté!');
      end;

      if errLst.Count > 24 then
      begin
        ShowMessage(errLst.GetText);
        errLst.Clear;
      end;

    until FindNext(ff) <> 0;
    SysUtils.FindClose(ff);

    Application.ProcessMessages;;

    if fc > 0 then
      frmQueue.lbFiles.Items.AddStrings(ffiles);

    if dc > 0 then
      for i := 0 to dc -1 do
      begin
        lbFName.Caption := 'Analyse de "'+fdirs[i]+'".';
        RecurScanFiles(fdirs[i],errLst);
      end;

    result := boolean(fc and dc);
  end;
end;

procedure TfrmMain.ApplicationPropertiesDropFilesAction(Sender: TObject; const FileNames: array of String);
var i: integer;
  fa: longint;
  errLst: TStringList;
begin
  errLst := TStringList.Create;
  btCryptFile.Enabled := false;
  btDecryptFile.Enabled := false;
  btOpenFile.Enabled := true;
  lbFName.Caption := 'Aucun';
  frmQueue.lbFiles.Items.Clear;
  totSz := 0;
  pbFile.Position:=0;
  pbMultiFiles.Position:=0;
  curSz:=0;
  curIdx:=0;
  lastPos:=0;
  StatusBar1.SimpleText:='Analyse des fichiers en cours ...';
  if High(FileNames) > -1 then
  begin
    tmScanFiles.Enabled := true;
    for i := 0 to High(FileNames) do
    begin
      fa := FileGetAttr(FileNames[i]);
      if (FileNames[i] <> '') and FileExists(FileNames[i])
        {$IFDEF LINUX}and ((fa and faSymLink) <> faSymLink){$ENDIF}
        and ((fa and faDirectory) <> faDirectory) then
      begin
        frmQueue.lbFiles.Items.Add(FileNames[i]);
        totSz := totSz + FileSize(FileNames[i]);
      end else if (conf.AppConfig.recurMode = true)
        and ((fa and faDirectory) = faDirectory)
        {$IFDEF LINUX}and ((fa and faSymLink) <> faSymLink){$ENDIF} then
      begin
        if FileIsWritable(FileNames[i]) then
          RecurScanFiles(FileNames[i],errLst)
        else begin
          errLst.Add('Le dossier '+FileNames[i]+' n''a pu être ajouté!');
          errLst.Add('L''application ne peut pas y écrire.');
        end;
      end else
        errLst.Add('Le fichier '+FileNames[i]+' n''est pas autorisé!');
      Application.ProcessMessages;
      if errLst.Count > 24 then
      begin
        ShowMessage(errLst.GetText);
        errLst.Clear;
      end;
    end;
    tmScanFiles.Enabled := false;

    if errLst.Count > 0 then
    begin
      ShowMessage(errLst.GetText);
      errLst.Clear;
    end;

    if frmQueue.lbFiles.Count > 0 then
    begin
      frmQueue.lbFiles.Selected[0] := true;
      frmQueue.MenuItem5.Enabled := true;
{$IFDEF MSWINDOWS}
      if frmQueue.lbFiles.Count > 49999 then
      begin
        StatusBar1.SimpleText := 'Ouverure de la file d''attente ...';
        MessageDlg('Attention!',
          'Un grand nombre de fichiers viennent d''être ajoutés.'+#13#10
         +'La fenêtre "File d''attente" pourrait mettre un long moment à s''ouvrir,'+#13#10
         +'Dans le même temps l''application semblera gelé, il n''en est rien!'+#13#10
         +'Veuillez Patienter, merci.',mtWarning,[mbOk],0);
      end;
{$ENDIF}

      frmQueue.Visible := true;
    end;
  end;

  if (frmQueue.lbFiles.Items.Count > 0) then
  begin
    btCryptFile.Enabled := true;
    btDecryptFile.Enabled := true;
    btOpenFile.Enabled := true;

    StatusBar1.SimpleText := 'Prêt à crypter/décrypter!';
    lbFName.Caption := frmQueue.lbFiles.Items[0];
    lbNbFiles.Caption:='Fichier 1 de '+IntToStr(frmQueue.lbFiles.Count);
    lbFileSize.Caption:='Taille : '+IntToStr(ceil(FileSize(frmQueue.lbFiles.Items[0])/1024))+' ko. sur '+IntToStr(ceil(TotSz/1024))+' ko.';
  end else
    StatusBar1.SimpleText := 'Rien à traiter!';;
end;



procedure TfrmMain.OpenFileClick(Sender: TObject);
var res:boolean;
begin
  res := OpenDialog1.Execute;
  if res = true then
  begin
    if (OpenDialog1.FileName <> '') and FileExists(OpenDialog1.FileName) then
    begin
      btCryptFile.Enabled := true;
      btDecryptFile.Enabled := true;
      frmQueue.lbFiles.Clear;
      lbFName.Caption := OpenDialog1.FileName;
      OpenDialog1.FileName := '';
      frmQueue.lbFiles.Items.Add(lbFName.Caption);

      StatusBar1.SimpleText := 'Prêt à crypter/décrypter!';
      lbNbFiles.Caption:='Fichier 1 de 1';
      lbFileSize.Caption:='Taille : '+IntToStr(ceil(FileSize(lbFName.Caption)/1024))+' ko.';
    end else
    begin
      StatusBar1.SimpleText := 'Ce fichier n''est pas valide!';
      OpenDialog1.FileName := '';
      //lbFName.Caption := '';
    end;
  end else if not FileExists(lbFName.Caption) then
  begin
    StatusBar1.SimpleText := 'Opération annulé!';
    OpenDialog1.FileName := '';
    //lbFName.Caption := '';
  end
end;

procedure TfrmMain.CryptFileClick(Sender: TObject);
var i: integer;
  currFz: Int64;
  tc: TThreadComponent;
  prms: TProgressRecord;
  emptyFh: THandle;
begin
  try
    AbortAllOps := false;
    AllowDropFiles := false;
    btCryptFile.Enabled := false;
    btDecryptFile.Enabled := false;
    btOpenFile.Enabled := false;
    btGenKey.Enabled := false;
    edKey.Enabled := false;
    cbxCipher.Enabled := false;
    frmQueue.MainMenu1.Items.Enabled := false;
    frmQueue.PopupMenu1.Items.Enabled := false;
    btCancelFile.Visible := true;
    btCancelAll.Visible := true;
    frmQueue.lbFiles.Enabled := false;

    pbFile.Position := 0;
    pbMultiFiles.Position := 0;
    curSz := 0;
    curIdx := 0;
    lastPos := 0;

    prms.Mode := dcpMCrypt;
    prms.EKey := TEncryption(cbxCipher.ItemIndex);
    prms.Key := edKey.Text;
    prms.lastPos := 0;
    prms.CurSz := 0;
    prms.TotSz := totSz;
    prms.pbFile := pbFile;
    prms.pbMultiFiles := pbMultiFiles;

    StatusBar1.SimpleText := 'Le fichier est en cours de cryptage !';
    pbFile.Visible := true;
    if frmQueue.lbFiles.Count > 1 then pbMultiFiles.Visible := true;
    Screen.Cursor := crHourglass;
    Cipher := cbxCipher.ItemIndex;

    for i := 0 to frmQueue.lbFiles.Count-1 do
    begin
      if i < frmQueue.lbFiles.Count then
      begin
        frmQueue.lbFiles.Selected[i] := true;
        if i > 0 then
          frmQueue.lbFiles.Selected[i-1] := false;
        lbFName.Caption := frmQueue.lbFiles.Items[i];
        lbNbFiles.Caption:='Fichier '+IntToStr(i+1)+' de '+IntToStr(frmQueue.lbFiles.Count);
        lbFileSize.Caption:='Taille : '+IntToStr(ceil(FileSize(frmQueue.lbFiles.Items[i])/1024))+' ko. sur '+IntToStr(ceil(TotSz/1024))+' ko.';
      end; //else problem (should disable dropdown & queue files list in crypting stage)

      currFz:=FileSize(frmQueue.lbFiles.Items[i]);
      {* call thread *}
      if (not AbortAllOps) and (currFz > 0) then
      begin

        prms.FilenameIn := lbFName.Caption;
        prms.FilenameOut := lbFName.Caption+'.crypt';

        tc := TThreadComponent.Create(TComponent(Sender),true, prms);
        tc.FThr.Start;
        while not tc.FThr.Finished do
        begin
          if AbortOp or AbortAllOps then
          begin
            AbortOp := true;
            tc.FThr.CancelOperation();
          end;
          Application.ProcessMessages;
        end;
        WaitForThreadTerminate(tc.FThr.Handle,3300);
        prms.lastPos := tc.FThr.ProgRec.lastPos;
        prms.CurSz := tc.FThr.ProgRec.CurSz;
        tc.FThr.Free;
        if AbortOp then
        begin
          DeleteFile(PChar(lbFName.Caption+'.crypt'));
          AbortOp := false;
          Continue;
        end else
        if conf.AppConfig.overwrite = true then
        begin
          DeleteFile(PChar(lbFName.Caption));
          RenameFile(lbFName.Caption+'.crypt',lbFName.Caption);
        end;
        conf.AddToHisto(Integer(prms.EKey),onFile,prms.Key,ExtractFileName(prms.FilenameIn));
      end else if currFz = 0 then
      begin
        if conf.AppConfig.overwrite <> true then
        begin
          emptyFh := FileCreateUTF8(lbFName.Caption+'.crypt');
          FileClose(emptyFh);
        end;
        conf.AddToHisto(Integer(prms.EKey),onFile,prms.Key,ExtractFileName(prms.FilenameIn));
      end else break;
    end;
  finally
    btOpenFile.Enabled := true;
    btCryptFile.Enabled := true;
    btDecryptFile.Enabled := true;
    btGenKey.Enabled := true;
    edKey.Enabled := true;
    cbxCipher.Enabled := true;
    frmMain.AllowDropFiles := true;
    frmQueue.MainMenu1.Items.Enabled := true;
    frmQueue.PopupMenu1.Items.Enabled := true;
    btCancelFile.Visible := false;
    btCancelAll.Visible := false;
    frmQueue.lbFiles.Enabled := true;

    if(AbortOp xor AbortAllOps) then
      StatusBar1.SimpleText := 'Une ou plusieurs opérations ont été annulé!'
    else StatusBar1.SimpleText := 'Le cryptage a été effectué avec succées!';
    pbFile.Position := 0;
    pbMultiFiles.Position := 0;
    pbFile.Visible := false;
    pbMultiFiles.Visible := false;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfrmMain.DecryptFileClick(Sender: TObject);
var
  i,suffix: integer;
  tc: TThreadComponent;
  prms: TProgressRecord;
  currFz: Int64;
  emptyFh: THandle;
begin
  try
    AbortAllOps := false;
    AllowDropFiles := false;
    btCryptFile.Enabled := false;
    btDecryptFile.Enabled := false;
    btOpenFile.Enabled := false;
    btGenKey.Enabled := false;
    edKey.Enabled := false;
    cbxCipher.Enabled := false;
    frmQueue.MainMenu1.Items.Enabled := false;
    frmQueue.PopupMenu1.Items.Enabled := false;
    btCancelFile.Visible := true;
    btCancelAll.Visible := true;
    frmQueue.lbFiles.Enabled := false;

    pbFile.Position:=0;
    pbMultiFiles.Position:=0;
    curSz:=0;
    curIdx:=0;
    lastPos:=0;

    prms.Mode := dcpMDecrypt;
    prms.EKey := TEncryption(cbxCipher.ItemIndex);
    prms.Key := edKey.Text;
    prms.lastPos := 0;
    prms.CurSz := 0;
    prms.TotSz := totSz;
    prms.pbFile := pbFile;
    prms.pbMultiFiles := pbMultiFiles;

    StatusBar1.SimpleText := 'Le fichier est en cours de décryptage !';
    pbFile.Visible := true;
    if frmQueue.lbFiles.Count > 1 then pbMultiFiles.Visible := true;
    Screen.Cursor := crHourglass;
    Cipher := cbxCipher.ItemIndex;

    for i := 0 to frmQueue.lbFiles.Count-1 do
    begin
      if i < frmQueue.lbFiles.Count then
      begin
        frmQueue.lbFiles.Selected[i] := true;
        if i > 0 then
          frmQueue.lbFiles.Selected[i-1] := false;
        lbFName.Caption := frmQueue.lbFiles.Items[i];
        lbNbFiles.Caption:='Fichier '+IntToStr(i+1)+' de '+IntToStr(frmQueue.lbFiles.Count);
        lbFileSize.Caption:='Taille : '+IntToStr(ceil(FileSize(frmQueue.lbFiles.Items[i])/1024))+' ko. sur '+IntToStr(ceil(TotSz/1024))+' ko.';
      end;

      currFz:=FileSize(frmQueue.lbFiles.Items[i]);
      {* call thread *}
      if (not AbortAllOps) and (currFz > 0) then
      begin
        prms.FilenameIn := lbFName.Caption;
        suffix := Pos('.crypt',prms.FilenameIn);
        if suffix > 1 then
        prms.FilenameOut := Copy(lbFName.Caption,1,suffix-1)
        else
        prms.FilenameOut := lbFName.Caption+'.decrypt';

        tc := TThreadComponent.Create(TComponent(Sender),true, prms);
        tc.FThr.Suspended:=false;
        while not tc.FThr.Finished do
        begin
          if AbortOp or AbortAllOps then
          begin
            AbortOp := true;
            tc.FThr.CancelOperation();
          end;
          Application.ProcessMessages;
        end;
        prms.lastPos := tc.FThr.ProgRec.lastPos;
        prms.CurSz := tc.FThr.ProgRec.CurSz;
        WaitForThreadTerminate(tc.FThr.Handle,3300);
        tc.FThr.Free;
        if AbortOp then
        begin
          DeleteFile(PChar(lbFName.Caption+'.decrypt'));
          AbortOp := false;
          Continue;
        end else
        if conf.AppConfig.overwrite = true then
        begin
          DeleteFile(PChar(lbFName.Caption));
          RenameFile(lbFName.Caption+'.decrypt',lbFName.Caption);
        end;
        conf.AddToHisto(Integer(prms.EKey),onFile,prms.Key,ExtractFileName(prms.FilenameIn));
      end else if currFz = 0 then
      begin
        if conf.AppConfig.overwrite <> true then
        begin
          FileCreateUTF8(lbFName.Caption+'.decrypt');
          FileClose(emptyFh);
        end;
        conf.AddToHisto(Integer(prms.EKey),onFile,prms.Key,ExtractFileName(prms.FilenameIn));
      end else break;
    end;
  finally
    btOpenFile.Enabled := true;
    btCryptFile.Enabled := true;
    btDecryptFile.Enabled := true;
    btGenKey.Enabled := true;;
    edKey.Enabled := true;
    cbxCipher.Enabled := true;
    frmMain.AllowDropFiles := true;
    frmQueue.MainMenu1.Items.Enabled := true;
    frmQueue.PopupMenu1.Items.Enabled := true;
    btCancelFile.Visible := false;
    btCancelAll.Visible := false;
    frmQueue.lbFiles.Enabled := true;

    if(AbortOp xor AbortAllOps) then
      StatusBar1.SimpleText := 'Une ou plusieurs opérations ont été annulé!'
    else StatusBar1.SimpleText := 'Le décryptage a été effectué avec succées!';
    pbFile.Position := 0;
    pbMultiFiles.Position := 0;
    pbFile.Visible := false;
    pbMultiFiles.Visible := false;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfrmMain.Quitter1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmMain.Fermer1Click(Sender: TObject);
begin
 lbFName.Caption := '';
 StatusBar1.SimpleText := 'Fichier fermé!';
end;

procedure TfrmMain.btRAZStrClick(Sender: TObject);
begin
  mmStr.Text := '';
  btCryptStr.Enabled := false;
  btDecryptStr.Enabled := false;
end;


procedure TfrmMain.CryptStrClick(Sender: TObject);
var cryptStr,txtStr: string;
begin
  try
    StatusBar1.SimpleText := 'La chaine est en cours de cryptage !';
    Screen.Cursor := crHourglass;
    case TEncryption(cbxCipher2.ItemIndex) of
      eBf      :
        begin
          DCP_blowfish := TDCP_blowfish.Create(nil,nil);
          Key := edKey2.Text;
          DCP_blowfish.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          cryptStr := DCP_blowfish.EncryptString(mmStr.Text);
          mmStr.Text := txtStr;
          DCP_blowfish.Burn;
          DCP_blowfish.Free;
          mmStr.Text := cryptStr;
        end;
      eRdl:
        begin
          DCP_rijndael := TDCP_rijndael.Create(nil,nil);
          Key := edKey2.Text;
          DCP_rijndael.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          cryptStr := DCP_rijndael.EncryptString(txtStr);
          mmStr.Text := txtStr;
          DCP_rijndael.Burn;
          DCP_rijndael.Free;
          mmStr.Text := cryptStr;
        end;
      e3des:
        begin
          DCP_3des := TDCP_3des.Create(nil,nil);
          Key := edKey2.Text;
          DCP_3des.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          cryptStr := DCP_3des.EncryptString(mmStr.Text);
          mmStr.Text := txtStr;
          DCP_3des.Burn;
          DCP_3des.Free;
          mmStr.Text := cryptStr;
        end;
      eRc6:
        begin
          DCP_rc6 := TDCP_rc6.Create(nil,nil);
          Key := edKey2.Text;
          DCP_rc6.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          cryptStr := DCP_rc6.EncryptString(mmStr.Text);
          mmStr.Text := txtStr;
          DCP_rc6.Burn;
          DCP_rc6.Free;
          mmStr.Text := cryptStr;
        end;
      eSerpent:
        begin
          DCP_serpent := TDCP_serpent.Create(nil,nil);
          Key := edKey2.Text;
          DCP_serpent.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          cryptStr := DCP_serpent.EncryptString(mmStr.Text);
          mmStr.Text := txtStr;
          DCP_serpent.Burn;
          DCP_serpent.Free;
          mmStr.Text := cryptStr;
        end;
      eIce2:
        begin
          DCP_ice2 := TDCP_ice2.Create(nil,nil);
          Key := edKey2.Text;
          DCP_ice2.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          cryptStr := DCP_ice2.EncryptString(mmStr.Text);
          mmStr.Text := txtStr;
          DCP_ice2.Burn;
          DCP_ice2.Free;
          mmStr.Text := cryptStr;
        end;
      eTwofish:
        begin
          DCP_twofish := TDCP_twofish.Create(nil,nil);
          Key := edKey2.Text;
          DCP_twofish.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          cryptStr := DCP_twofish.EncryptString(mmStr.Text);
          mmStr.Text := txtStr;
          DCP_twofish.Burn;
          DCP_twofish.Free;
          mmStr.Text := cryptStr;
        end;
      eCast256:
        begin
          DCP_cast256 := TDCP_cast256.Create(nil,nil);
          Key := edKey2.Text;
          DCP_cast256.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          cryptStr := DCP_cast256.EncryptString(mmStr.Text);
          mmStr.Text := txtStr;
          DCP_cast256.Burn;
          DCP_cast256.Free;
          mmStr.Text := cryptStr;
        end;
    end;
    conf.AddToHisto(cbxCipher2.ItemIndex,onString,Key,Copy(mmStr.Text,1,50));
    StatusBar1.SimpleText := 'La chaine à été crypté avec succées!';
    finally
      Screen.Cursor := crDefault;
  end;
end;

procedure TfrmMain.DecryptStrClick(Sender: TObject);
var decryptStr, txtStr: string;
begin
  try
    StatusBar1.SimpleText := 'La chaine est en cours de décryptage !';
    Screen.Cursor := crHourglass;
    case TEncryption(cbxCipher2.ItemIndex) of
      eBf      :
        begin
          DCP_blowfish := TDCP_blowfish.Create(nil,nil);
          Key := edKey2.Text;
          DCP_blowfish.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          decryptStr := DCP_blowfish.DecryptString(txtStr);
          mmStr.Text := txtStr;
          DCP_blowfish.Burn;
          DCP_blowfish.Free;
          mmStr.Text := decryptStr;
        end;
      e3des:
        begin
          DCP_3des := TDCP_3des.Create(nil,nil);
          Key := edKey2.Text;
          DCP_3des.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          decryptStr := DCP_3des.DecryptString(txtStr);
          mmStr.Text := txtStr;
          DCP_3des.Burn;
          DCP_3des.Free;
          mmStr.Text := decryptStr;
        end;
      eRdl:
        begin
          DCP_rijndael := TDCP_rijndael.Create(nil,nil);
          Key := edKey2.Text;
          DCP_rijndael.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          decryptStr := DCP_rijndael.DecryptString(txtStr);
          mmStr.Text := txtStr;
          DCP_rijndael.Burn;
          DCP_rijndael.Free;
          mmStr.Text := decryptStr;
        end;
      eRc6:
        begin
          DCP_rc6 := TDCP_rc6.Create(nil,nil);
          Key := edKey2.Text;
          DCP_rc6.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          decryptStr := DCP_rc6.DecryptString(txtStr);
          mmStr.Text := txtStr;
          DCP_rc6.Burn;
          DCP_rc6.Free;
          mmStr.Text := decryptStr;
        end;
      eSerpent:
        begin
          DCP_serpent := TDCP_serpent.Create(nil,nil);
          Key := edKey2.Text;
          DCP_serpent.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          decryptStr := DCP_serpent.DecryptString(txtStr);
          mmStr.Text := txtStr;
          DCP_serpent.Burn;
          DCP_serpent.Free;
          mmStr.Text := decryptStr;
        end;
      eIce2:
        begin
          DCP_ice2 := TDCP_ice2.Create(nil,nil);
          Key := edKey2.Text;
          DCP_ice2.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          decryptStr := DCP_ice2.DecryptString(txtStr);
          mmStr.Text := txtStr;
          DCP_ice2.Burn;
          DCP_ice2.Free;
          mmStr.Text := decryptStr;
        end;
      eTwofish:
        begin
          DCP_twofish := TDCP_twofish.Create(nil,nil);
          Key := edKey2.Text;
          DCP_twofish.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          decryptStr := DCP_twofish.DecryptString(txtStr);
          mmStr.Text := txtStr;
          DCP_twofish.Burn;
          DCP_twofish.Free;
          mmStr.Text := decryptStr;
        end;
      eCast256:
        begin
          DCP_cast256 := TDCP_cast256.Create(nil,nil);
          Key := edKey2.Text;
          DCP_cast256.InitStr(Key,TDCP_sha1);
          txtStr := mmStr.Text;
          decryptStr := DCP_cast256.DecryptString(txtStr);
          mmStr.Text := txtStr;
          DCP_cast256.Burn;
          DCP_cast256.Free;
          mmStr.Text := decryptStr;
        end;
    end;
    conf.AddToHisto(cbxCipher.ItemIndex,onString,Key,Copy(mmStr.Text,1,50));
    StatusBar1.SimpleText := 'La chaine à été décrypté avec succées!';
    finally
      Screen.Cursor := crDefault;
   end;
end;

procedure TfrmMain.closeCancelClick(Sender: TObject);
begin
  btCryptFile.Enabled := false;
  btDecryptFile.Enabled := false;
  btOpenFile.Enabled := true;
  lbFName.Caption := 'Aucun';
end;

procedure TfrmMain.Button1Click(Sender: TObject);
begin
  frmConfigure.Visible := true;
  frmMain.Enabled := false;
end;

procedure TfrmMain.mmStrChange(Sender: TObject);
begin
  if mmStr.Text <> '' then
  begin
    btCryptStr.Enabled := true;
    btDecryptStr.Enabled := true;
  end;
end;

procedure TfrmMain.btGenKeyClick(Sender: TObject);
begin
  Key := GenKey(192);
  edKey.Text := Key;
end;

procedure TfrmMain.btGenKey2Click(Sender: TObject);
begin
  Key := GenKey(192);
  edKey2.Text := Key;
end;

procedure TfrmMain.tmScanFilesTimer(Sender: TObject);
var prgstr: string = '   ...   ';
begin
    StatusBar1.SimpleText:='Analyse des fichiers en cours '+Copy(prgstr,PosChPrg,3);
    if (PosChPrg = 1) then PosChPrg := 6 else Dec(PosChPrg);
end;


initialization
  {$I CPgUnit1.lrs}
end.
