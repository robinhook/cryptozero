program CryptoZero;

{$mode objfpc}{$H+}

uses
{$IFDEF UNIX} cmem,{$IFDEF UseCThreads} cthreads,{$ENDIF}{$ENDIF}
  Classes, SysUtils, Forms, Interfaces, CPgUnit1, CPgUnit2, CPgUnit3,
  CPgUnit4, CPgUnit5, CPgUnit6, CPSocks, AppCfgUnit, LazUTF8,LCLVersion;

{$R *.res}

begin
    Application.Initialize;
    conf.LoadConfig();
    Application.CreateForm(TfrmMain, frmMain);
    Application.CreateForm(TfrmNetwork, frmNetwork);
    Application.CreateForm(TfrmHisto, frmHisto);
    Application.CreateForm(TfrmQueue, frmQueue);
    Application.CreateForm(TfrmQuestion, frmQuestion);
    Application.CreateForm(TfrmConfigure, frmConfigure);
    Application.Run;
end.

