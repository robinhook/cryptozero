unit CPgUnit4;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Menus, Process, AppCfgUnit, LazUTF8;

type

  { TfrmQueue }

  TfrmQueue = class(TForm)
    lbFiles: TListBox;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem9: TMenuItem;
    PopupMenu1: TPopupMenu;
    procedure FormCreate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbFilesClick(Sender: TObject);
    procedure lbFilesMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
  private
    { private declarations }
    lbFy:integer;
  public
    { public declarations }
    {$IFDEF MSWINDOWS}
    procedure lbFilesDrawItem(Control: TWinControl; Index: Integer;
      ARect: TRect; State: TOwnerDrawState);
    {$ENDIF}
  end;

var
  frmQueue: TfrmQueue;

implementation

uses Math,CPgUnit1;

{ TfrmQueue }
procedure TfrmQueue.FormCreate(Sender: TObject);
var i: integer;
begin
  if (conf.AppConfig.formstate[4].top > 0) and (conf.AppConfig.formstate[4].left > 0) then
  begin
    Top := conf.AppConfig.formstate[4].top;
    Left := conf.AppConfig.formstate[4].left;
  end;
  if (conf.AppConfig.formstate[4].width > 0) and (conf.AppConfig.formstate[4].height > 0) then
  begin
    Width := conf.AppConfig.formstate[4].width;
    Height := conf.AppConfig.formstate[4].height;
  end;
  {$IFDEF MSWINDOWS}
  lbFiles.OnDrawItem := @lbFilesDrawItem;
  {$ENDIF}
  lbFiles.Clear;
  if (conf.AppConfig.saveList = true) and (conf.ListFile.Count > 0) then
    for i := 0 to conf.ListFile.Count - 1 do
      lbFiles.Items.Add(conf.ListFile[i]);
end;

procedure TfrmQueue.FormHide(Sender: TObject);
var i: integer;
begin
  conf.AppConfig.formstate[4].top := Top;
  conf.AppConfig.formstate[4].left := Left;
  conf.AppConfig.formstate[4].width := Width;
  conf.AppConfig.formstate[4].height := Height;

  conf.ListFile.Clear;
  if lbFiles.Count > 0 then
    for i := 0 to lbFiles.Count - 1 do
      conf.ListFile.Add(lbFiles.Items[i]);
end;


procedure TfrmQueue.FormResize(Sender: TObject);
begin
  lbFiles.Width := ClientWidth;
  lbFiles.Height := ClientHeight - 2;
end;

procedure TfrmQueue.FormShow(Sender: TObject);
begin
  if lbFiles.Items.Count = 0 then
  begin
    MenuItem5.Enabled:=false;
  end;
end;

procedure TfrmQueue.lbFilesClick(Sender: TObject);
var i,sel: integer;
begin
  sel := -1;
  for i:= 0 to lbFiles.Count - 1 do
    if lbFiles.Selected[i] = true then
    begin
      sel := i;
      frmMain.lbFName.Caption := lbFiles.Items[i];
      frmMain.lbNbFiles.Caption:='Fichier '+IntToStr(i+1)+' de '+IntToStr(lbFiles.Count);
      frmMain.lbFileSize.Caption:='Taille : '+IntToStr(Ceil64(FileSize(lbFiles.Items[i])/1024))+' ko. sur '+IntToStr(ceil(frmMain.TotSz/1024))+' ko.';
      break;
    end;
  if (sel = -1) and (lbFiles.Count > 0) then
    lbFiles.Selected[0] := true;
end;

{$IFDEF MSWINDOWS}
procedure TfrmQueue.lbFilesDrawItem(Control: TWinControl; Index: Integer;
  ARect: TRect; State: TOwnerDrawState);
begin
  Application.ProcessMessages;
end;
{$ENDIF}

procedure TfrmQueue.lbFilesMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  lbFy := Y;
end;

procedure TfrmQueue.MenuItem1Click(Sender: TObject);
var i,sel: integer;
  tmp: string;
begin
  for i:= 0 to lbFiles.Count - 1 do
    if lbFiles.Selected[i] = true then
    begin
      sel := i;
      break;
    end;
  if sel < 1 then exit;
  tmp := lbFiles.Items[i-1];
  lbFiles.Items[i-1] := lbFiles.Items[i];
  lbFiles.Items[i] := tmp;
  lbFiles.Selected[i-1] := true;
end;

procedure TfrmQueue.MenuItem2Click(Sender: TObject);
var i,sel: integer;
  tmp: string;
begin
  for i:= 0 to lbFiles.Count - 1 do
    if lbFiles.Selected[i] = true then
    begin
      sel := i;
      break;
    end;
  if sel >= lbFiles.Count - 1 then exit;
  tmp := lbFiles.Items[i+1];
  lbFiles.Items[i+1] := lbFiles.Items[i];
  lbFiles.Items[i] := tmp;
  lbFiles.Selected[i+1] := true;
end;

procedure TfrmQueue.MenuItem3Click(Sender: TObject);
var i: integer;
begin
  for i:= 0 to lbFiles.Count - 1 do
    if lbFiles.Selected[i] = true then
    begin
      lbFiles.Items.Delete(i);
    end;

  frmMain.totSz := 0;
  for i := 0 to lbFiles.Count - 1 do
    if (conf.ListFile[i] <> '') and FileExists(conf.ListFile[i]) then
      Inc(frmMain.totSz,FileSize(conf.ListFile[i]));

  if lbFiles.Count = 0 then
  begin
    MenuItem5.Enabled := false;
    frmMain.ResetFileGroupBox();
  end else MenuItem5.Enabled := true;
end;

procedure TfrmQueue.MenuItem4Click(Sender: TObject);
var OpDialog: TopenDialog;
  f:TextFile;
  buffer:string;
begin
  OpDialog := TOpenDialog.Create(self);
  OpDialog.Filter:='Fichier Texte (*.txt)|*.txt';
  if (OpDialog.Execute) and (FileExists(OpDialog.FileName)) then
  begin
    conf.ListFile.Clear;
    frmQueue.lbFiles.items.Clear;
    AssignFile(f,OpDialog.FileName);
    Reset(f);
    while not eof(f) do
    begin
      ReadLn(f,buffer);
      if (FileExists(buffer) or FileExists(buffer+'.crypt') or FileExists(buffer+'.decrypt')) then
      begin
        conf.ListFile.Add(buffer);
        lbFiles.Items.Add(buffer);
      end;
    end;
    CloseFile(f);
  end;
  OpDialog.Free;
  frmMain.btCryptFile.Enabled := true;
  frmMain.btDecryptFile.Enabled := true;
end;

procedure TfrmQueue.MenuItem5Click(Sender: TObject);
var SvDialog:TSaveDialog;
  f: TextFile;
  i:integer;
begin
  SvDialog := TSaveDialog.Create(self);
  SvDialog.Filter:='Fichier Texte (*.txt)|*.txt';
  if SvDialog.Execute then
  begin
    //Exporte la liste
    AssignFile(f,SvDialog.FileName);
    Rewrite(f);
    for i := 0 to lbFiles.Items.Count -1 do
    begin
      Writeln(f,lbFiles.Items[i]);
    end;
    CloseFile(f);
  end;

  SvDialog.Free;
end;

procedure TfrmQueue.MenuItem9Click(Sender: TObject);
var cl: string;
  p: TProcess;
begin
  p := TProcess.Create(nil);
{$ifdef UNIX}
  cl := 'xdg-open';
{$endif}
{$ifdef MSWINDOWS}
  cl := 'explorer.exe';
{$endif}
  cl := cl + ' "'+ExtractFilePath(lbFiles.Items[lbFiles.GetIndexAtY(lbFy)])+'"';
  p.CommandLine := cl;
  p.Execute;
  p.Free;
end;

procedure TfrmQueue.PopupMenu1Popup(Sender: TObject);
var i: integer;
begin
  if lbFiles.GetIndexAtY(lbFy) <> -1 then
  begin
    for i := 0 to PopupMenu1.Items.Count -1 do
      PopupMenu1.Items[i].Enabled := true;
    lbFiles.Selected[lbFiles.GetIndexAtY(lbFy)] := true;
  end else
    for i := 0 to PopupMenu1.Items.Count -1 do
      PopupMenu1.Items[i].Enabled := false;
end;

initialization
  {$I CPgUnit4.lrs}
end.

