unit CPgUnit6;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, LCLType, Forms, Controls, Graphics, Dialogs,
  StdCtrls, CPSocks, AppCfgUnit, LazUTF8;

type

  { TfrmQuestion }

  TfrmQuestion = class(TForm)
    btAccept: TButton;
    btRefus: TButton;
    lbFilename: TLabel;
    procedure btAcceptClick(Sender: TObject);
    procedure btRefusClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormHide(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    QAnswer: LongInt;
    procedure Question;
    procedure InitQuestion;
    procedure FinQuestion;
  end;

var
  frmQuestion: TfrmQuestion;

implementation

procedure TfrmQuestion.InitQuestion;
var q: string;
  hb: TNetHeaderBuffer;
begin
  hb := recvSock.GetHeaderBuffer;
  q := Format('Le fichier %s de %s octets'+#13#10+'En provenance de %s.'+#13#10
             +'Acceptez-vous la réception?',
             [hb.filename,IntToStr(hb.filesize),recvSock.Host]);
  lbFilename.Caption := q;

  Visible := true;
end;

procedure TfrmQuestion.FinQuestion;
begin
  Visible := false;
  RTLeventSetEvent(recvSock.waitForAnswer);
end;

procedure TfrmQuestion.btAcceptClick(Sender: TObject);
begin
  QAnswer := 2;
  FinQuestion;
end;

procedure TfrmQuestion.btRefusClick(Sender: TObject);
begin
  QAnswer := 1;
  FinQuestion;
end;

procedure TfrmQuestion.FormClose(Sender: TObject; var CloseAction: TCloseAction
  );
begin
  QAnswer := 1;
  FinQuestion;
end;

procedure TfrmQuestion.FormCreate(Sender: TObject);
begin
  if (conf.AppConfig.formstate[6].top > 0) and (conf.AppConfig.formstate[6].left > 0) then
  begin
    Top := conf.AppConfig.formstate[6].top;
    Left := conf.AppConfig.formstate[6].left;
  end;
  if (conf.AppConfig.formstate[6].width > 0) and (conf.AppConfig.formstate[6].height > 0) then
  begin
    Width := conf.AppConfig.formstate[6].width;
    Height := conf.AppConfig.formstate[6].height;
  end;

end;

procedure TfrmQuestion.FormHide(Sender: TObject);
begin
  conf.AppConfig.formstate[6].top := Top;
  conf.AppConfig.formstate[6].left := Left;
end;

procedure TfrmQuestion.Question;
begin
  QAnswer := -1;
  InitQuestion;
end;

initialization
  {$I CPgUnit6.lrs}
end.

