#
# Detects and parses the architecture
#

ARCH='i386'

case "$ARCH" in

 "i686") ARCH="i386";;

 "i586") ARCH="i386";;

 "i486") ARCH="i386";;

esac

echo "Target architecture: $ARCH"

#
# Detects and parses the OS
#

OS="linux"

echo "Target operating system: $OS"


#
# Command line to build the sofware
#

ppc386 -va/etc/fpc386.cfg -S2cgi -OG1 -gl -WG -vewnhi -l -Fu/usr/lib/lazarus/components/opengl/gtk2x11/ -Fi/usr/lib/lazarus/components/opengl/gtk2x11/include/ -Fu/usr/lib/lazarus/components/jpeg/ -Fu/usr/lib/lazarus/lcl/units/$ARCH-$OS/ -Fu/usr/lib/lazarus/lcl/units/$ARCH-$OS/ -Fu/usr/lib/lazarus/lcl/units/$ARCH-$OS/gtk2/ -Fu/usr/lib/lazarus/packager/units/$ARCH-$OS/ -Fu. -o./magnifier -dLCL -dLCLgtk2 CryptoZero.lpr
