#
# Don´t use "rm -rf" in here, because you should only remove the files you created
#
rm -f /usr/share/cryptozero/in.gif
rm -f /usr/share/cryptozero/out.gif
rm -f /usr/share/cryptozero/cryptozero.ico

rm -f /usr/share/cryptozero/CryptoZero

rm -f /usr/bin/CryptoZero

rmdir /usr/share/cryptozero
