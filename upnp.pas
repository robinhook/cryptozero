unit UPnP;

{$mode objfpc}{$H+}

interface

uses {$IFDEF UNIX}BaseUnix,{$ENDIF}
  Classes, SysUtils, Forms, Sockets, RegExpr, AppCfgUnit, LazUTF8;

type
  TMappedPort = record
      Name,Protocol: string;
      InternalIP,ExternalIP: string;
      InternalPort,ExternalPort: Word;
      LeaseTime: integer;
  end;
  PMappedPort = ^TMappedPort;

  TListDevice = class
    uuid,location,urlsvc:string;
  public
    mappedPorts:TList;
  end;

  TUPnP = class
  protected
    LanRoutedIP:String;
    HndBCast, HndSoap: Longint;
    SAddr   : TInetSockAddr;
  public
    class var FLDev: TStringList;
    FMsg, FListDevice:TStringList;
    constructor Create(IP: string; lbMsg: TStringList);
    destructor Destroy; override;
    procedure searchDevice;
    procedure restoreDevices;
    function setMapping(uuid,rulename:string; port:Word): boolean;
    function unsetMapping(uuid:string; rulename:string): boolean;
    function listMapping(uuid:string):string;
  end;

implementation

uses URIParser;

constructor TUPnP.Create(IP:string; lbMsg: TStringList);
begin
  inherited Create;
  FListDevice := TStringList.Create;
  FMsg := lbMsg;

  if TUPnP.FLDev = nil then
    TUPnP.FLDev := TStringList.Create
  else restoreDevices;

  LanRoutedIP := IP;
end;

destructor TUPnP.Destroy;
begin
  if (TUPnP.FLDev <> nil) and Assigned(TUPnP.FLDev) and (TUPnP.FLDev.Count = 0) then
    FreeAndNil(TUPnP.FLDev);

  FListDevice.Free;
  inherited Destroy;
end;

procedure TUPnP.restoreDevices;
var i: longint;
begin
  if (TUPnP.FLDev <> nil) and (FListDevice <> nil) and (TUPnP.FLDev.Count > 0) then
    for i := 0 to TUPnP.FLDev.Count -1 do
      FListDevice.AddObject(TUPnP.FLDev[i],TUPnP.FLDev.Objects[i]);
end;

function TUPnP.setMapping(uuid,rulename: string; port:Word): boolean;
var q: String;
  rl,idx,httpcode: Longint;
  resp: array[0..4095]of byte;
  re:TRegExpr;
  ld:TListDevice;
  url:TURI;
begin
  Result := false;
  idx := FListDevice.IndexOf(uuid);
  if idx = -1 then Exit;
  ld := TListDevice(FListDevice.Objects[idx]);
  FMsg.Add('Create TCP socket to add a mapped port ...');
  HndSoap := fpsocket(AF_INET,SOCK_STREAM,IPPROTO_IP);
  if HndSoap <> SOCKET_ERROR then
  begin
    FillByte(SAddr,SizeOf(SAddr),0);
    url := ParseURI(ld.location,true);
    SAddr.sin_family := AF_INET;

    SAddr.sin_port := htons(url.port);
    SAddr.sin_addr := StrToNetAddr(url.Host);
    if fpconnect(HndSoap,@SAddr,sizeof(saddr)) = SOCKET_ERROR then
      WriteLn('Connect: '+SysErrorMessage(socketerror));

    q := 'POST '+ld.urlsvc+' HTTP/1.1'+#13#10 +
      'HOST: '+url.Host+ #13#10 +
      'SOAPACTION: "urn:schemas-upnp-org:service:WANIPConnection:1#AddPortMapping"'+#13#10+
      'CONTENT-TYPE: text/xml; charset="utf-8"'+#13#10+
      #13#10+
      '<?xml version="1.0" ?>'+
      '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'+
      '<s:Body>'+
      '<u:AddPortMapping xmlns:u="urn:schemas-upnp-org:service:WANIPConnection:1">' +
      '<NewRemoteHost></NewRemoteHost><NewExternalPort>'+IntToStr(port)+'</NewExternalPort>'+
      '<NewProtocol>TCP</NewProtocol>' +
      '<NewInternalPort>'+IntToStr(port)+'</NewInternalPort>'+
      '<NewInternalClient>'+LanRoutedIP+'</NewInternalClient>'+
      '<NewEnabled>1</NewEnabled>'+
      '<NewPortMappingDescription>CZ_'+rulename+'</NewPortMappingDescription>'+
      '<NewLeaseDuration>0</NewLeaseDuration></u:AddPortMapping>'+
      '</s:Body></s:Envelope>';

    FMsg.Add('Adding port ...');
    if fpSend(HndSoap,@q[1],Length(q),0) = SOCKET_ERROR then
      FMsg.Add('Send: '+SysErrorMessage(socketerror));

    repeat
      httpcode := 0;
      FillByte(resp,4096,0);
      rl := fprecv(HndSoap,@resp[0],4096,0);
      if rl = SOCKET_ERROR then
        FMsg.Add('Recv: '+SysErrorMessage(socketerror));
      re := TRegExpr.Create;
      re.Expression := 'HTTP/1.\d{1} (\d){3} [a-zA-Z ]+'+#13#10;
      if re.Exec(PChar(@resp[0])) then
        TryStrToInt(re.Match[1],httpcode);
      re.Free;
    until rl = 0;

    fpshutdown(HndSoap,2);
    CloseSocket(HndSoap);

    case httpcode of
      500: Exit;
      200:
      begin
        TUpnP.FLDev.AddObject(FListDevice[idx],FListDevice.Objects[idx]);
        Result := true;
      end;
    end;
  end else
    FMsg.Add('Socket: '+SysErrorMessage(socketerror));

end;

function TUPnP.unsetMapping(uuid,rulename: string): boolean;
var q: String;
  rl,httpcode,idx: Longint;
  resp: array[0..4095]of byte;
  re:TRegExpr;
  ld:TListDevice;
  url:TURI;
begin
  Result := false;
  idx := FListDevice.IndexOf(uuid);
  if idx = -1 then Exit;
  ld := TListDevice(FListDevice.Objects[idx]);
  FMsg.Add('Create TCP socket to remove a mapped port ...');
  HndSoap := fpsocket(AF_INET,SOCK_STREAM,IPPROTO_IP);
  if HndSoap <> SOCKET_ERROR then
  begin
    FillByte(SAddr,SizeOf(SAddr),0);
    url := ParseURI(ld.location,true);
    SAddr.sin_family := AF_INET;

    SAddr.sin_port := htons(url.port);
    SAddr.sin_addr := StrToNetAddr(url.Host);
    if fpconnect(HndSoap,@SAddr,sizeof(saddr)) = SOCKET_ERROR then
      WriteLn('Connect: '+SysErrorMessage(socketerror));

    q := 'POST '+ld.urlsvc+' HTTP/1.1'+#13#10 +
      'HOST: '+url.Host+ #13#10 +
      'SOAPACTION: "urn:schemas-upnp-org:service:WANIPConnection:1#DeletePortMapping"'+#13#10+
      'CONTENT-TYPE: text/xml; charset="utf-8"'+#13#10+
      #13#10+
      '<?xml version="1.0" ?>'+
      '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'+
      '<s:Body>'+
      '<u:DeletePortMapping xmlns:u="urn:schemas-upnp-org:service:WANIPConnection:1">' +
      '<NewRemoteHost></NewRemoteHost><NewExternalPort>'+IntToStr(0)+'</NewExternalPort>'+
      '<NewProtocol>TCP</NewProtocol>' +
      '</u:DeletePortMapping></s:Body></s:Envelope>';

    FMsg.Add('Removing port ...');
    if fpsend(HndSoap,@q[1],Length(q),0) = SOCKET_ERROR then
      FMsg.Add('Send: '+SysErrorMessage(socketerror));

    repeat
      httpcode := 0;
      FillByte(resp,4096,0);
      rl := fprecv(HndSoap,@resp[0],4096,0);
      if rl = SOCKET_ERROR then
        FMsg.Add('Recv: '+SysErrorMessage(socketerror));
      re := TRegExpr.Create;
      re.Expression := 'HTTP/1.\d{1} (\d){3} [a-zA-Z ]+'+#13#10;
      if re.Exec(PChar(@resp[0])) then
        TryStrToInt(re.Match[1],httpcode);
      re.Free;
    until rl = 0;

    fpshutdown(HndSoap,2);
    CloseSocket(HndSoap);

    if httpcode = 500 then Exit;

    case httpcode of
      500: Exit;
      200:
      begin
        TUpnP.FLDev.Delete(idx);
        Result := true;
      end;
    end;
  end else
    FMsg.Add('Socket: '+SysErrorMessage(socketerror));
end;

function TUPnP.listMapping(uuid: string):string;
var q: String;
  rl,i,idx,httpcode: Longint;
  tmv: {$IFDEF UNIX}Timeval{$ELSE}Longint{$ENDIF};
  resp: array[0..4095]of byte;
  re:TRegExpr;
  mp:PMappedPort;
  ld: TListDevice;
  url:TURI;
begin
  i := 0;
  idx := FListDevice.IndexOf(uuid);
  if idx = -1 then Exit;
  ld := TListDevice(FListDevice.Objects[idx]);
  if ld.mappedPorts = nil then
    ld.mappedPorts := TList.Create;
  FMsg.Add('Request listing for mapped ports ...');
  Application.ProcessMessages;
  repeat
    HndSoap := fpsocket(AF_INET,SOCK_STREAM,IPPROTO_IP);
    if HndSoap <> SOCKET_ERROR then
    begin
      url := ParseURI(ld.location);
      FillByte(SAddr,SizeOf(SAddr),0);
      SAddr.sin_family := AF_INET;
      SAddr.sin_port := htons(url.Port);
      SAddr.sin_addr := StrToNetAddr(url.Host);

      if fpconnect(HndSoap,@SAddr,sizeof(saddr)) = SOCKET_ERROR then
      begin
        FMsg.Add('Connect: '+SysErrorMessage(socketerror));
        Application.ProcessMessages;
      end;
{$IFDEF UNIX}
      tmv.tv_sec  := 7;
      tmv.tv_usec := 0;
{$ELSE}
      tmv := 3000;
{$ENDIF}
      if fpsetsockopt(HndSoap,SOL_SOCKET,SO_RCVTIMEO,@tmv,SizeOf(tmv)) = SOCKET_ERROR then
      begin
        FMsg.Add('[TUPnP.listMappings] Option RecvTmo: '+SysErrorMessage(socketerror));
        Application.ProcessMessages;
      end;

      q := 'POST '+ld.urlsvc+' HTTP/1.1'+#13#10 +
        'HOST: '+url.Host+ #13#10 +
        'SOAPACTION: "urn:schemas-upnp-org:service:WANIPConnection:1#GetGenericPortMappingEntry"'+#13#10+
        'CONTENT-TYPE: text/xml; charset="utf-8"'+#13#10+
        #13#10+
        '<?xml version="1.0"?>'+
        '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'+
        '<s:Body>'+
        '<u:GetGenericPortMappingEntry xmlns:u="urn:schemas-upnp-org:service:WANIPConnection:1">'+
        '<NewPortMappingIndex>'+IntToStr(i)+'</NewPortMappingIndex>'+
        '</u:GetGenericPortMappingEntry>'+
        '</s:Body></s:Envelope>';

      if fpsend(HndSoap,@q[1],Length(q),0) = SOCKET_ERROR then
      begin
        FMsg.Add('Send: '+SysErrorMessage(socketerror));
        Exit;
      end;

      repeat
        httpcode := 0;
        FillByte(resp,4096,0);
        rl := fprecv(HndSoap,@resp[0],4096,0);
        if rl = SOCKET_ERROR then
        begin
          FMsg.Add('Recv: '+SysErrorMessage(socketerror));
          Exit;
        end;
        //FMsg.Add('Response('+IntToStr(rl)+'): '+PChar(@resp[0]));

        re := TRegExpr.Create;
        re.Expression := 'HTTP/1.[01] (\d+) [a-zA-Z ]+'+#13#10;
        if re.Exec(PChar(@resp[0])) then
          TryStrToInt(re.Match[1],httpcode);
//        FOnMsgMeth('DBG: '+IntToStr(i)+'|'+re.Match[1]+'/'+IntToStr(httpcode));
        re.Free;

        if httpcode = 200 then
        begin
          re := TRegExpr.Create;
          re.Expression := '<([a-zA-Z0-9]+?)>([a-zA-Z0-9_ .-]+?)</[a-zA-Z0-9]+?>';
          re.InputString:= PChar(@resp[0]);
          if re.Exec(re.InputString) then
          begin
            new(mp);
            repeat
              if re.Match[1] = 'NewPortMappingDescription' then
                mp^.name := re.Match[2]
              else if re.Match[1] = 'NewLeaseDuration' then
                mp^.LeaseTime := StrToInt(re.Match[2])
              else if re.Match[1] = 'NewRemoteHost' then
                mp^.ExternalIP := re.Match[2]
              else if re.Match[1] = 'NewExternalPort' then
                mp^.ExternalPort := StrToInt(re.Match[2])
              else if re.Match[1] = 'NewProtocol' then
                mp^.protocol := re.Match[2]
              else if re.Match[1] = 'NewInternalPort' then
                mp^.InternalPort := StrToInt(re.Match[2])
              else if re.Match[1] = 'NewInternalClient' then
                mp^.InternalIP := re.Match[2];

            until not re.ExecNext;

            if ld.mappedPorts.IndexOf(mp) = -1 then
              ld.mappedPorts.Add(mp)
            else Dispose(mp);
            FMsg.Add('Rule found "'+mp^.name+'" on ip/port "'+mp^.InternalIP+':'+IntToStr(mp^.InternalPort)+'".');
            Application.ProcessMessages;
          end;
          re.Free;
        end;

      until (rl = 0) or (httpcode = 500);
      Inc(i);

      fpshutdown(HndSoap,2);
      CloseSocket(HndSoap);
    end else
      FMsg.Add('Socket: '+SysErrorMessage(socketerror));
  until httpcode <> 500;

end;

procedure TUPnP.searchDevice;
var q: String;
  rl,optval,i,idx,trl: Longint;
  tmv: {$IFDEF UNIX}Timeval{$ELSE}Longint{$ENDIF};
  tox: SockAddr;
  dev:TListDevice;
  resp: array[0..4095]of byte;
  re:TRegExpr;
begin
  FMsg.Add('Creating Broadcast socket for SSDP request ...');
  Application.ProcessMessages;
  FListDevice.Clear;

  HndBCast := fpsocket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
  if HndBCast <> SOCKET_ERROR then
  begin
    SAddr.sin_port := htons(0);
    SAddr.sin_addr.s_addr := htonl(INADDR_ANY);

    optval := 1;
    if fpSetSockOpt(HndBCast, SOL_SOCKET, SO_BROADCAST, @optval,sizeof(optval)) = SOCKET_ERROR then
    begin
      FMsg.Add('[TUPnP.searchDevice] Set Option Broadcast: '+SysErrorMessage(socketerror));
      Application.ProcessMessages;
    end;

    if fpBind(HndBCast,@SAddr,sizeof(saddr)) = SOCKET_ERROR then
    begin
      FMsg.Add('[TUPnP.searchDevice] Bind: '+SysErrorMessage(socketerror));
      Application.ProcessMessages;
    end;

{$IFDEF UNIX}
    tmv.tv_sec  := 7;
    tmv.tv_usec := 0;
{$ELSE}
    tmv := 3000;
{$ENDIF}
    if fpsetsockopt(HndBCast,SOL_SOCKET,SO_RCVTIMEO,@tmv,SizeOf(tmv)) = SOCKET_ERROR then
    begin
      FMsg.Add('[TUPnP.searchDevice] Option RecvTmo: '+SysErrorMessage(socketerror));
      Application.ProcessMessages;
    end;

    //search for intenet gateway device
    tox.sa_family:= AF_INET;
    tox.sin_addr := StrToNetAddr('239.255.255.250');
    tox.sin_port := htons(1900);
    q := 'M-SEARCH * HTTP/1.1'+ #13#10 +
         'HOST: 239.255.255.250:1900'+ #13#10 +
         'ST:upnp:rootdevice'+ #13#10 +
         'MAN:"ssdp:discover"'+ #13#10 +
         'MX:3'+ #13#10#13#10;

    FMsg.Add('SSDP request ...');
    Application.ProcessMessages;
    rl := fpsendto(HndBCast,@q[1],Length(q),0,@tox,SizeOf(tox));
    if rl = SOCKET_ERROR then
    begin
      FMsg.Add('[TUPnP.searchDevice] Send: '+SysErrorMessage(socketerror));
      Application.ProcessMessages;
    end;

    FMsg.Add('Waiting for response ...');
    Application.ProcessMessages;
    trl := 0;
    rl := 0;
    repeat
      rl := fprecv(HndBCast,@resp[rl],4096,0);
      Inc(trl,rl);
    until rl <= 0;

    if trl <= 0 then
    begin
      FMsg.Add('No response!');
      Exit;
    end;

    fpshutdown(HndBCast,2);
    CloseSocket(HndBCast);

  end else
  begin
    FMsg.Add('[TUPnP.searchDevice] Socket: '+SysErrorMessage(socketerror));
    Exit;
  end;

  re := TRegExpr.Create;
  re.Expression := 'Location:\s*(http.+?:\d+?/.+?)'+#13#10+'.*USN:\s*uuid:(.+InternetGatewayDevice.+?)'+#13#10;
  re.InputString:=String(PChar(@resp[0]));
  i:=1;
  if re.ExecPos(i) then
  begin
    repeat
      dev := TListDevice.Create;
      dev.location := re.Match[1];
      dev.uuid := re.Match[2];
      dev.urlsvc:='/WANIPConnection';
      idx := FListDevice.Add(dev.uuid);
      FListDevice.Objects[idx] := dev;
      FMsg.Add('Found device uuid: '+dev.uuid);
      Inc(i);
    until re.ExecPos(i);
  end else
    FMsg.Add('Nothing found!');
  re.Free;
end;

end.

