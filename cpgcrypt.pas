unit CPgCrypt;

{$mode objfpc}{$H+}

interface

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  cmem, // the c memory manager is on some systems much faster for multi-threading
{$ENDIF}{$ENDIF}
  Classes, SysUtils, DCPCrypt2, DCPrijndael, DCPdes, DCPsha1, DCPblowfish, DCPserpent, DCPtwofish, DCPrc6, DCPcast256, DCPice,AppCfgUnit;

Type
    TCryptThread = class(TThread)
    private
      ThrFinished: boolean;
      Mode: integer;
      Sz,Pos: LongWord;
      Key,FilenameIn,FilenameOut: string;
      EKey:TEncryption;
      procedure UpdateStatus;
    protected
      procedure CryptFile;
      procedure DecryptFile;
      procedure Execute; override;
    public
      DCPCipher : TDCP_cipher;
      Constructor Create(CreateSuspended : boolean;Operation, TKey: integer; CKey,FIn,Fout: string);
      procedure CancelOperation();
      procedure Progress(position,size:LongWord);
      procedure ThreadTerminate(Sender:Tobject);
      property Finished: boolean read ThrFinished;
    end;


implementation
  uses CPgUnit1;

  constructor TCryptThread.Create(CreateSuspended : boolean;Operation, TKey: integer; CKey,FIn,Fout: string);
  begin
    OnTerminate := @ThreadTerminate;
    Mode := Operation;
    EKey := TEncryption(TKey);
    Key := CKey;
    FilenameIn := FIn;
    FilenameOut := Fout;
    inherited Create(CreateSuspended);
  end;

  procedure TCryptThread.ThreadTerminate(Sender: TObject);
  begin
    ThrFinished := true;
  end;

  // Synchronized with main thread method
  procedure TCryptThread.UpdateStatus;
  begin
    if (Pos > 0) and (Sz > 0) then
    begin
      if pos < Form1.lastPos then Form1.lastPos := 0;
      Form1.pbFile.Position := Round((Pos / Sz)*100);
      Form1.curSz := Form1.curSz + (Pos - Form1.lastPos);
      Form1.lastPos := Pos;
      if((Form1.curSz > 0) and (Form1.totSz > 0)) then
        Form1.pbMultiFiles.Position := Round((Form1.curSz / Form1.totSz)*100);
    end else
    begin
      Form1.pbFile.Position := 0;
      Form1.lastPos := 0;
    end;
  end;

  procedure TCryptThread.Progress(position,size: LongWord);
  begin
    Sz := size;
    Pos := position;
    Synchronize(@UpdateStatus);
  end;

  procedure TCryptThread.CryptFile;
  var readStr,writeStr:TFileStream;
  begin
    case EKey of
      eBf      :
        begin
          DCPCipher := TDCP_blowfish.Create(nil);
          TDCP_blowfish(DCPCipher).OnMethProgress := @Progress;
        end;
      eRdl:
        begin
          DCPCipher := TDCP_rijndael.Create(nil);
          TDCP_rijndael(DCPCipher).OnMethProgress := @Progress;
        end;
      e3des:
        begin
          DCPCipher := TDCP_3des.Create(nil);
          TDCP_3des(DCPCipher).OnMethProgress := @Progress;
        end;
      eCast256:
        begin
          DCPCipher := TDCP_cast256.Create(nil);
          TDCP_cast256(DCPCipher).OnMethProgress := @Progress;
        end;
      eIce2:
        begin
          DCPCipher := TDCP_ice2.Create(nil);
          TDCP_ice2(DCPCipher).OnMethProgress := @Progress;
        end;
      eRc6:
        begin
          DCPCipher := TDCP_rc6.Create(nil);
          TDCP_rc6(DCPCipher).OnMethProgress := @Progress;
        end;
      eSerpent:
        begin
          DCPCipher := TDCP_serpent.Create(nil);
          TDCP_serpent(DCPCipher).OnMethProgress := @Progress;
        end;
      eTwofish:
        begin
          DCPCipher := TDCP_twofish.Create(nil);
          TDCP_twofish(DCPCipher).OnMethProgress := @Progress;
        end;
    end;
    DCPCipher.InitStr(Key,TDCP_sha1);
    readStr := TFileStream.Create(FilenameIn,fmOpenRead,fmShareCompat);
    writeStr := TFileStream.Create(FilenameOut,fmCreate,fmShareExclusive);
    DCPCipher.EncryptStream(readStr,writeStr,readStr.Size);
    DCPCipher.Burn;
    DCPCipher.Free;
    writeStr.Free;
    readStr.Free;
  end;

  procedure TCryptThread.DecryptFile();
  var readStr,writeStr:TFileStream;
  begin
    case EKey of
    eBf      :
      begin
        DCPCipher := TDCP_blowfish.Create(nil);
        TDCP_blowfish(DCPCipher).OnMethProgress := @Progress;
      end;
    eRdl:
      begin
        DCPCipher := TDCP_rijndael.Create(nil);
        TDCP_rijndael(DCPCipher).OnMethProgress := @Progress;
      end;
    e3des:
      begin
        DCPCipher := TDCP_3des.Create(nil);
        TDCP_3des(DCPCipher).OnMethProgress := @Progress;
      end;
    eCast256:
      begin
        DCPCipher := TDCP_cast256.Create(nil);
        TDCP_cast256(DCPCipher).OnMethProgress := @Progress;
      end;
    eIce2:
      begin
        DCPCipher := TDCP_ice2.Create(nil);
        TDCP_ice2(DCPCipher).OnMethProgress := @Progress;
      end;
    eRc6:
      begin
        DCPCipher := TDCP_rc6.Create(nil);
        TDCP_rc6(DCPCipher).OnMethProgress := @Progress;
      end;
    eSerpent:
      begin
        DCPCipher := TDCP_serpent.Create(nil);
        TDCP_serpent(DCPCipher).OnMethProgress := @Progress;
      end;
    eTwofish:
      begin
        DCPCipher := TDCP_twofish.Create(nil);
        TDCP_twofish(DCPCipher).OnMethProgress := @Progress;
      end;
    end;
    DCPCipher.InitStr(Key,TDCP_sha1);
    readStr := TFileStream.Create(FilenameIn,fmOpenRead,fmShareCompat);
    writeStr := TFileStream.Create(FilenameOut,fmCreate,fmShareExclusive);
    DCPCipher.DecryptStream(readStr,writeStr,readStr.Size);
    DCPCipher.Burn;
    DCPCipher.Free;
    writeStr.Free;
    readStr.Free;
  end;

  procedure TCryptThread.CancelOperation();
  begin
    DCPCipher.CancelOperation();
  end;

  procedure TCryptThread.Execute;
  begin
    if (Mode = Crypt) then
      CryptFile()
    else if (Mode = Decrypt) then
      DecryptFile();
//?    Terminate();
  end;

end.

