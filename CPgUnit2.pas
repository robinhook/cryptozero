unit CPgUnit2;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, DCPdes, DCPrijndael, DCPtwofish, DCPserpent, DCPice,
  AppCfgUnit, LazUTF8;

type

  { TfrmConfigure }

  TfrmConfigure = class(TForm)
    btDlPath: TButton;
    cbRecursiveAdd: TCheckBox;
    cbHisto: TCheckBox;
    cbOverwrite: TCheckBox;
    cbSaveKey: TCheckBox;
    cbShowKey: TCheckBox;
    cbListFile: TCheckBox;
    cbContextMenu: TCheckBox;
    cbUPnP: TCheckBox;
    gbHisto: TGroupBox;
    GroupBox2: TGroupBox;
    btSave: TButton;
    btCancel: TButton;
    Label1: TLabel;
    lbPath: TLabel;
    sdDownload: TSelectDirectoryDialog;
    procedure btDlPathClick(Sender: TObject);
    procedure cbShowKeyChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btSaveClick(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure FormHide(Sender: TObject);
  private
    { Déclarations privées }
{$IFDEF MSWINDOWS}
  {$IFDEF CZCONTEXT}procedure cbContextMenuChange(Sender: TObject);{$ENDIF}
{$ENDIF}
  public
    { Déclarations publiques }
    updateForm1:boolean;
  end;

  // Dlls :
{$IFDEF MSWINDOWS}
{$IFDEF CZCONTEXT}
  {$IFDEF WIN64}
  procedure DllGetClassObject; stdcall; external 'CZContext64';
  procedure DllCanUnloadNow; stdcall; external 'CZContext64';
  procedure DllRegisterServer; stdcall; external 'CZContext64';
  procedure DllUnregisterServer; stdcall; external 'CZContext64';
  {$ELSE}
  procedure DllGetClassObject; stdcall; external 'CZContext32';
  procedure DllCanUnloadNow; stdcall; external 'CZContext32';
  procedure DllRegisterServer; stdcall; external 'CZContext32';
  procedure DllUnregisterServer; stdcall; external 'CZContext32';
  {$ENDIF}
{$ENDIF}
{$ENDIF}

var
  frmConfigure: TfrmConfigure;

implementation

uses CPgUnit1,CPgUnit3,CPgUnit5;

procedure TfrmConfigure.cbShowKeyChange(Sender: TObject);
begin
  if cbShowKey.Checked then
  begin
    frmMain.edKey.PasswordChar := #0;
    frmMain.edKey2.PasswordChar := #0;
    frmNetwork.edKey.PasswordChar := #0;
  end else
  begin
    frmMain.edKey.PasswordChar := '*';
    frmMain.edKey2.PasswordChar := '*';
    frmNetwork.edKey.PasswordChar := '*';
  end;
  frmMain.edKey.Refresh;
  frmMain.edKey2.Refresh;
  frmNetwork.edKey.Refresh;
end;

procedure TfrmConfigure.btDlPathClick(Sender: TObject);
begin
  if sdDownload.Execute then
  begin
    lbPath.Caption := sdDownload.FileName;
    conf.AppConfig.downloadPath := sdDownload.FileName;
  end;
end;

{$IFDEF MSWINDOWS}
  {$IFDEF CZCONTEXT}
procedure TfrmConfigure.cbContextMenuChange(Sender: TObject);
begin
  if cbContextMenu.Checked then
    DllRegisterServer
  else
    DllUnregisterServer;
end;
  {$ENDIF}
{$ENDIF}

procedure TfrmConfigure.FormCreate(Sender: TObject);
begin
  cbSaveKey.Checked := conf.AppConfig.saveKey;
  cbHisto.Checked := conf.AppConfig.Historize;
  cbOverwrite.Checked := conf.AppConfig.overwrite;
  cbShowKey.Checked := conf.AppConfig.showkey;
  cbListFile.Checked := conf.AppConfig.saveList;
  cbRecursiveAdd.Checked := conf.AppConfig.recurMode;
{$IFDEF CZCONTEXT}
  {$IFDEF MSWINDOWS}
  cbContextMenu.Checked := conf.AppConfig.ctxMenu;
  cbContextMenu.OnChange := @cbContextMenuChange;
 {$ENDIF}
{$ELSE}
  cbContextMenu.Enabled := false;
{$ENDIF}
  cbUPnP.Checked := conf.AppConfig.upnp;
  if cbShowKey.Checked then
  begin
    frmMain.edKey.PasswordChar := #0;
    frmMain.edKey2.PasswordChar := #0;
    conf.AppConfig.showKey := true;
  end else
  begin
    frmMain.edKey.PasswordChar := '*';
    frmMain.edKey2.PasswordChar := '*';
    conf.AppConfig.showKey := false;
  end;

  if (conf.AppConfig.formstate[2].top > 0) and (conf.AppConfig.formstate[2].left > 0) then
  begin
    Top := conf.AppConfig.formstate[2].top;
    Left := conf.AppConfig.formstate[2].left;
  end;

{*  if (conf.AppConfig.formstate[2].width > 0) and (conf.AppConfig.formstate[2].height > 0) then
  begin
    Width := conf.AppConfig.formstate[2].width;
    Height := conf.AppConfig.formstate[2].height;
  end;
*}
  if DirectoryExists(conf.AppConfig.downloadPath) then
    lbPath.Caption := conf.AppConfig.downloadPath;

end;

procedure TfrmConfigure.btSaveClick(Sender: TObject);
begin
  conf.AppConfig.saveKey := cbSaveKey.Checked;
  conf.AppConfig.historize := cbHisto.Checked;
  conf.AppConfig.overwrite := cbOverwrite.Checked;
  conf.AppConfig.showKey := cbShowKey.Checked;
  conf.AppConfig.saveList := cbListFile.Checked;
  conf.AppConfig.recurMode := cbRecursiveAdd.Checked;
{$IFDEF MSWINDOWS}
  {$IFDEF CZCONTEXT}conf.AppConfig.ctxMenu := cbContextMenu.Checked;{$ENDIF}
{$ENDIF}
  conf.AppConfig.upnp := cbUPnP.Checked;
  conf.AppConfig.downloadPath := lbPath.Caption;
  conf.SaveConfig;
  Close;
end;

procedure TfrmConfigure.btCancelClick(Sender: TObject);
begin
  cbSaveKey.Checked := conf.AppConfig.saveKey;
  cbHisto.Checked := conf.AppConfig.Historize;
  cbOverwrite.Checked := conf.AppConfig.overwrite;
  cbShowKey.Checked := conf.AppConfig.showKey;
  cbListFile.Checked := conf.AppConfig.saveList;
  cbRecursiveAdd.Checked := conf.AppConfig.recurMode;
{$IFDEF MSWINDOWS}
  {$IFDEF CZCONTEXT}cbContextMenu.Checked := conf.AppConfig.ctxMenu;{$ENDIF}
{$ENDIF}
  cbUPnP.Checked := conf.AppConfig.upnp;
  Close;
end;

procedure TfrmConfigure.FormHide(Sender: TObject);
begin
  conf.AppConfig.formstate[2].top := Top;
  conf.AppConfig.formstate[2].left := Left;
  conf.AppConfig.formstate[2].width := Width;
  conf.AppConfig.formstate[2].height := Height;

  frmMain.Enabled := true;
end;


initialization
  {$I CPgUnit2.lrs}
end.
