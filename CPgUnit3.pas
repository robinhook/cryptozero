unit CPgUnit3;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  Grids, Menus, DCPThread, AppCfgUnit, LazUTF8;

type

  { TfrmHisto }

  TfrmHisto = class(TForm)
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    PopupMenu1: TPopupMenu;
    sgHisto: TStringGrid;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure sgHistoMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { private declarations }
    sgHy,sgHx: integer;
  public
    { public declarations }
  end; 

var
  frmHisto: TfrmHisto;

implementation

{ TfrmHisto }

procedure TfrmHisto.FormActivate(Sender: TObject);
var i,cnt: integer;
begin
  if conf.AppConfig.historize = true then
  begin
    cnt := Length(conf.HistoKey);
    if cnt > 0 then
    begin
      sgHisto.RowCount := cnt+1;
      for i := 0 to cnt-1 do
      begin
        case TEncryption(conf.HistoKey[i].mode) of
          eBf      : sgHisto.Rows[i+1][0] := 'BlowFish';
          e3Des    : sgHisto.Rows[i+1][0] := '3DES';
          eRdl     : sgHisto.Rows[i+1][0] := 'Rijndael';
          eCast256 : sgHisto.Rows[i+1][0] := 'Cast 256';
          eIce2    : sgHisto.Rows[i+1][0] := 'Ice 2';
          eRc6     : sgHisto.Rows[i+1][0] := 'Rc 6';
          eSerpent : sgHisto.Rows[i+1][0] := 'Serpent';
          eTwofish : sgHisto.Rows[i+1][0] := 'TwoFish';
        end;
        sgHisto.Rows[i+1][1] := conf.HistoKey[i].key;
        if conf.HistoKey[i].used = onString then
          sgHisto.Rows[i+1][2] := 'Chaine: ' else sgHisto.Rows[i+1][2] := 'Fichier: ';
        sgHisto.Rows[i+1][2] := sgHisto.Rows[i+1][2] + conf.HistoKey[i].info;
        sgHisto.Rows[i+1][3] := DateToStr(conf.HistoKey[i].date);
      end;
    end;
  end;
  if conf.AppConfig.showkey = false then
    sgHisto.Columns[1].Visible:=false
    else sgHisto.Columns[1].Visible:=true;
end;

procedure TfrmHisto.FormCreate(Sender: TObject);
var i,cnt: integer;
begin
  if (conf.AppConfig.formstate[3].top > 0) and (conf.AppConfig.formstate[3].left > 0) then
  begin
    Top := conf.AppConfig.formstate[3].top;
    Left := conf.AppConfig.formstate[3].left;
  end;
  if (conf.AppConfig.formstate[3].width > 0) and (conf.AppConfig.formstate[3].height > 0) then
  begin
    Width := conf.AppConfig.formstate[3].width;
    Height := conf.AppConfig.formstate[3].height;
  end;

    if conf.AppConfig.historize = true then
    begin
      cnt := Length(conf.HistoKey);
      if cnt > 0 then
      begin
        sgHisto.RowCount := cnt+1;
        for i := 0 to cnt-1 do
        begin
          case TEncryption(conf.HistoKey[i].mode) of
            eBf      : sgHisto.Rows[i+1][0] := 'BlowFish';
            e3Des    : sgHisto.Rows[i+1][0] := '3DES';
            eRdl     : sgHisto.Rows[i+1][0] := 'Rijndael';
          end;
          sgHisto.Rows[i+1][1] := conf.HistoKey[i].key;
          if conf.HistoKey[i].used = onString then
            sgHisto.Rows[i+1][2] := 'Chaine: ' else sgHisto.Rows[i+1][2] := 'Fichier: ';
          sgHisto.Rows[i+1][2] := sgHisto.Rows[i+1][2] + conf.HistoKey[i].info;
          sgHisto.Rows[i+1][3] := DateToStr(conf.HistoKey[i].date);
        end;
      end;
    end;

end;

procedure TfrmHisto.FormHide(Sender: TObject);
begin
  conf.AppConfig.formstate[3].top := Top;
  conf.AppConfig.formstate[3].left := Left;
  conf.AppConfig.formstate[3].width := Width;
  conf.AppConfig.formstate[3].height := Height;
end;

procedure TfrmHisto.FormResize(Sender: TObject);
begin
  sgHisto.Width := frmHisto.ClientWidth - 4;
  sgHisto.Height := frmHisto.ClientHeight - 6;
end;

procedure TfrmHisto.MenuItem1Click(Sender: TObject);

begin
  sgHisto.DeleteRow(sgHisto.Row);
  conf.DelFromHisto(sgHisto.Row);
end;

procedure TfrmHisto.MenuItem2Click(Sender: TObject);
var i: integer;
begin
  i := sgHisto.RowCount - 1;
  for i := i downto 1 do
    sgHisto.DeleteRow(i);
  sgHisto.RowCount := 2;
  SetLength(conf.HistoKey,0);
end;

procedure TfrmHisto.PopupMenu1Popup(Sender: TObject);
var aCol,aRow: integer;
begin
  aCol := -1;
  aRow := -1;
  sgHisto.MouseToCell(sgHx,sgHy,aCol,aRow);
  sgHisto.Row:=aRow;
end;

procedure TfrmHisto.sgHistoMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  sgHx := X;
  sgHy := Y;
end;

initialization
  {$I CPgUnit3.lrs}
end.

