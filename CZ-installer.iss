; -- 64Bit.iss --
; Demonstrates installation of a program built for the x64 (a.k.a. AMD64)
; architecture.
; To successfully run this installation and the program it installs,
; you must have the "x64" edition of Windows XP or Windows Server 2003.

; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=CryptoZero
AppVersion=1.2
DefaultDirName={pf}\CryptoZero
DefaultGroupName=CryptoZero
UninstallDisplayIcon={app}\CryptoZero.exe
Compression=lzma2
SolidCompression=yes
OutputDir=userdocs:CZ_Setups
; "ArchitecturesAllowed=x64" specifies that Setup cannot run on
; anything but x64.
ArchitecturesAllowed=x64
; "ArchitecturesInstallIn64BitMode=x64" requests that the install be
; done in "64-bit mode" on x64, meaning it should use the native
; 64-bit Program Files directory and the 64-bit view of the registry.
ArchitecturesInstallIn64BitMode=x64

[Files]
Source: "CryptoZero.exe"; DestDir: "{app}"; DestName: "CryptoZero.exe"
;Source: "MyProg.chm"; DestDir: "{app}"
;Source: "Readme.txt"; DestDir: "{app}"; Flags: isreadme

[Icons]
Name: "{group}\CryptoZero"; Filename: "{app}\CryptoZero.exe"
